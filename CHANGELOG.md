# Version 0.0.1

- Login / Logout users
- Handle recovery password
- Set default routes /alerts /teams /help /options
- Set Team view and show a list of teams
- Create a team
- Delete a team
