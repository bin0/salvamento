import { loadingBarMiddleware } from "react-redux-loading-bar";
import thunkMiddleware from "redux-thunk";

const promiseTypeSuffixes = ["REQUEST", "SUCCESS", "FAILURE"];

export default [thunkMiddleware, loadingBarMiddleware({ promiseTypeSuffixes })];
