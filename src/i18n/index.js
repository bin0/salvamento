import es from './es-ES/index.json';
import en from './en-EN/index.json';

export default { 'es-ES': es, 'en-EN': en, es, en, 'en-GB': en, 'en-US': en };
