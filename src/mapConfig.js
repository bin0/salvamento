import React from 'react';

export default {
  googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&v=3.exp&libraries=geometry,drawing,places`,
  loadingElement: <div style={{ height: `100%` }} />,
  containerElement: <div style={{ height: '92vh', width: '100vw' }} />,
  mapElement: <div style={{ height: `100%` }} />
};
