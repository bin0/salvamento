export default {
  onToggleOpen: ({ isOpen }) => () => ({
    isOpen: !isOpen
  }),
  onMarkerClustererClick: () => markerClusterer => {
    const clickedMarkers = markerClusterer.getMarkers();
    console.log(`Current clicked markers length: ${clickedMarkers.length}`);
    console.log(clickedMarkers);
  },
  onDblClick: (param1, param3) => param2 => {
    console.log("param1", param1);
    console.log("param2", param2);
    console.log("param3", param3);
  }
};
