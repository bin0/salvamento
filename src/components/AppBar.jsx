import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import PersonIcon from '@material-ui/icons/Person';
import { Tooltip } from '@material-ui/core';

import LogoAppBar from './LogoAppBar';
import AppBarMenu from './AppBarMenu';
import { actions as modalActions } from '../reducers/modal';
import { actions } from '../reducers/auth';
import styles, { SubtitleStyled, ConnectedUsersStyled } from './styles';

const MyAppBar = memo(function MyAppBar({
  authenticated,
  classes,
  color,
  push,
  pathname,
  logOut,
  subtitle,
  title,
  user,
  // unreadMessagesCount,
  count,
  i18n
}) {
  const handleBackHome = useCallback(() => push('/'), [push]);

  return (
    <AppBar position="sticky" color={color}>
      <Toolbar>
        <div onClick={handleBackHome}>
          <LogoAppBar />
        </div>
        <Typography variant="h6" color="inherit" className={classes.grow} onClick={handleBackHome}>
          {title}
          <SubtitleStyled>{subtitle}</SubtitleStyled>
        </Typography>
        {authenticated && (
          <Tooltip title="Usuarios conectados">
            <ConnectedUsersStyled>
              <Typography variant="caption">{count}</Typography>
              <PersonIcon />
            </ConnectedUsersStyled>
          </Tooltip>
        )}
        <AppBarMenu
          authenticated={authenticated}
          avatar={user.avatar}
          classes={classes}
          i18n={i18n}
          onLogout={logOut}
          push={push}
          pathname={pathname}
          // unreadMessagesCount={unreadMessagesCount}
        />
      </Toolbar>
    </AppBar>
  );
});

MyAppBar.propTypes = {
  authenticated: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  color: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  push: PropTypes.func.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  }),
  pathname: PropTypes.string.isRequired,
  logOut: PropTypes.func.isRequired,
  subtitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  // unreadMessagesCount: PropTypes.number.isRequired,
  user: PropTypes.object.isRequired
};

const StyledAppBar = withStyles(styles)(MyAppBar);

const mapStateToProps = ({ auth, user, chats, users, i18n }) => ({
  authenticated: auth.authenticated,
  user: user.local,
  // unreadMessagesCount: chats.unreadMessagesCount,
  count: users.count,
  i18n
});

const mapDispatchToProps = { ...modalActions, logOut: actions.logOut };

export default connect(mapStateToProps, mapDispatchToProps)(StyledAppBar);
