import React from 'react';
import Button from '@material-ui/core/Button';

const NotFoundPage = () => (
  <>
    <h1>400</h1>
    <p>Acceso no autorizado</p>
    <p>No tienes permiso para ver esta sección</p>
    <Button variant="contained" color="secondary">
      Inicio
    </Button>
  </>
);

export default NotFoundPage;
