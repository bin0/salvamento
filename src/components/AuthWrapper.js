import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hasPermission } from '../utils/permissions';
import { validateUserToken } from '../reducers/user/local';
import NotAllowed from './400';
import LoadingBar from 'react-redux-loading-bar';

function AuthWrapper(WrappedComponent, permissions) {
  class AuthWrapper extends React.Component {
    componentDidMount() {
      const { validateToken, authenticated, history } = this.props;
      const token = localStorage.getItem('wc-token');

      if (!token) {
        history.push('/login');
      } else if (!authenticated) {
        validateToken(token);
      }
    }

    render() {
      const hasAccess = this.props.userPermissions.some(hasPermission(permissions));
      if (!hasAccess) {
        return <NotAllowed />;
      }
      if (!this.props.authenticated) {
        return <LoadingBar className="redux-loading-bar" showFastActions />;
      }

      return <WrappedComponent {...this.props} />;
    }
  }

  AuthWrapper.propTypes = {
    authenticated: PropTypes.bool.isRequired,
    validateToken: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    })
  };

  const mapStateToProps = ({ auth: { authenticated }, user }) => ({
    authenticated,
    userPermissions: user.local.permissions
  });

  const mapDispatchToProps = { validateToken: validateUserToken };

  return connect(mapStateToProps, mapDispatchToProps)(AuthWrapper);
}

export default AuthWrapper;
