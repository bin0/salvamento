import styled from 'styled-components/macro';
import Button from '@material-ui/core/Button';

export default theme => ({
  grow: {
    flexGrow: 1
  },
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  },
  footerItem: {
    // width: '33%',
    padding: '1rem',
    border: '1px solid red'
  },
  h5: {},
  menuButton: {
    padding: 0
  },
  button: {
    margin: theme.spacing(1)
  },
  input: {
    display: 'none'
  },
  card: {
    padding: '2rem',
    display: 'flex',
    backgroundColor: theme.palette.secondary.main,
    flexDirection: 'column',
    color: theme.palette.primary.main
  },
  textField: {
    color: theme.palette.primary.main
  },
  captcha: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export const StyledContent = styled.div`
  margin: 1rem;
  position: absolute;
  top: 0;
  left: 0;
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }

  font-size: 0.9rem;

  p,
  a {
    color: #ccc;
    font-size: 0.8rem;
    font-weight: bold;
    margin-right: 1rem;
    text-decoration: none;
    letter-spacing: 0.25rem;
    text-shadow: 0.5px 0.5px #255968;
    text-transform: uppercase;

    &:hover {
      color: #61dafb;
    }
  }
`;

export const Box = styled.div`
  margin-right: 1rem;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    box-sizing: border-box;
    border: 0.1rem solid #282c33;
    box-shadow: 0px 0px 5px 0.1px #8ee7ff;
  }
`;

const sizes = {
  xl: '45px',
  lg: '30px',
  md: '20px',
  sm: '10px'
};

export const Box1 = styled.div`
  /* width: ${sizes.xl};
  height: ${sizes.xl}; */
  border-radius: 50%;
  border: 0.03rem solid #282c33;
`;
export const Box2 = styled.div`
  /* width: ${sizes.lg};
  height: ${sizes.lg}; */
  transform: rotate(45deg);
  border: 0.03rem solid #282c33;
`;
export const Box3 = styled.div`
  width: 20px;
  height: 20px;
  transform: rotate(30deg);
  border: 0.03rem solid #282c33;
`;
export const Box4 = styled.div`
  transform: rotate(15deg);
  width: 10px;
  height: 10px;
  border: 0.03rem solid #282c33;
`;

export const SubtitleStyled = styled.span`
  display: block;
  font-weight: 100;
  font-size: 0.7rem;
  letter-spacing: 0.1rem;
  line-height: 0.4rem;
  text-transform: uppercase;
  @media (min-width: 768px) {
    letter-spacing: 0.3rem;
  }
`;

export const FooterStyled = styled.footer`
  position: absolute;
  bottom: 0;
  right: 0;
  /* height: 0; */
  a {
    color: #ccc;
    font-size: 0.9rem;
    font-weight: 500;

    &:hover {
      color: #8ee7ff;
    }
  }
`;

export const RegisterContainerStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
`;

export const ConnectedUsersStyled = styled.div`
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 1rem;
`;

export const InfoWindowContainer = styled.div`
  border-left: 4px solid orange;
  opacity: 0.75;
  padding: 12px;

  div {
    font-size: 16px;
    color: #08233b;
  }
`;

export const StopMapButton = styled(Button)`
  position: absolute;
  top: 125px;
  left: 10px;
`;
