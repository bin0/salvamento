import React from 'react';
import Campaign from './Campaign';
import useCampaign from '../hooks/campaigns';

const Campaigns = () => {
  const { campaigns } = useCampaign();
  return (
    <div>
      <h2>Personas desaparecidas</h2>
      {campaigns.map(campaign => (
        <Campaign key={campaign.id} campaign={campaign} />
      ))}
    </div>
  );
};

export default Campaigns;
