import React from 'react';
import { InfoWindow } from 'react-google-maps';
import { InfoWindowContainer } from './styles';

const InfoWindowCustom = ({ location, i18n }) => {
  return (
    <InfoWindow position={location} options={{ closeBoxURL: '', enableEventPropagation: true }}>
      <InfoWindowContainer>
        <div>{i18n.get('app.map.meetingPoint')}</div>
      </InfoWindowContainer>
    </InfoWindow>
  );
};

export default InfoWindowCustom;
