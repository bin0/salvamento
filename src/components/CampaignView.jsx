import React, { useEffect } from 'react';
import MapComponent from './MapComponent';
import useLocation from '../hooks/location';

const CampaignView = () => {
  useEffect(() => {
    if (navigator.getBattery) {
      navigator.getBattery().then(result => {
        if (!result.charging && result.level <= 0.2) {
          console.log('No está en carga y te queda poca batería');
        } else {
          console.log('Está en carga o tiene batería suficiente');
        }
      });
    }
  }, []);
  const { watchPosition, clearWatching, location, loading, watching } = useLocation();

  return (
    <div className="App">
      {loading && <div>Cargando...</div>}
      {!loading && location.lat && (
        <>
          <MapComponent isMarkerShown location={location} />
        </>
      )}
      <div>
        {!watching ? (
          <div className="App">
            <header className="App-header">
              <button onClick={watchPosition}>Activar</button>
            </header>
          </div>
        ) : (
          <div>
            <button onClick={clearWatching}>Parar</button>
          </div>
        )}
      </div>
    </div>
  );
};

export default CampaignView;
