import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import defaultAvatarImg from '../assets/img/avatar.png';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { useCallback } from 'react';

const AppBarMenu = ({
  avatar: userAvatar,
  authenticated,
  classes,
  i18n,
  onLogout,
  pathname,
  push
  // unreadMessagesCount
}) => {
  const [avatar, setAvatar] = useState(defaultAvatarImg);

  useEffect(() => {
    if (userAvatar && userAvatar.data) {
      const data = Buffer.from(userAvatar.data).toString('base64');
      const contentType = userAvatar.data.contentType;
      setAvatar(`data:${contentType};base64,${data}`);
    }
  }, [userAvatar]);

  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = useCallback(event => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const handleLogout = useCallback(() => {
    setAnchorEl(null);
    onLogout();
  }, [onLogout]);

  const handleMenuClick = useCallback(
    route => {
      push(route);
      setAnchorEl(null);
    },
    [push]
  );

  const renderMenuItem = (route, name) =>
    pathname !== route && <MenuItem onClick={() => handleMenuClick(route)}>{name}</MenuItem>;

  return authenticated ? (
    <>
      <Badge color="primary">
        <IconButton className={classes.menuButton} onClick={handleClick} color="inherit" aria-label="Menu">
          <Avatar src={avatar} alt="user-profile-img" />
        </IconButton>
      </Badge>
      <Menu id="options-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        {renderMenuItem('/dashboard', i18n.get('dashboard'))}
        {renderMenuItem('/alerts', i18n.get('alerts'))}
        {renderMenuItem('/teams', i18n.get('teams'))}
        {renderMenuItem('/options', i18n.get('options'))}
        <MenuItem onClick={handleLogout}>{i18n.get('exit')}</MenuItem>
      </Menu>
    </>
  ) : (
    <>
      <IconButton className={classes.menuButton} onClick={handleClick} color="inherit" aria-label="Menu">
        <MoreVertIcon alt="user-profile" />
      </IconButton>
      <Menu id="options-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
        {renderMenuItem('/login', i18n.get('enter'))}
        {renderMenuItem('/login/register', i18n.get('register'))}
        {renderMenuItem('/help', i18n.get('help'))}
      </Menu>
    </>
  );
};

export default AppBarMenu;
