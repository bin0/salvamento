import React from 'react';

function logErrorToMyService(error, errorInfo) {
  console.error('ERROR =>', error);
  console.log('INFO =>', errorInfo);
}

class ErrorBoundaires extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, info: '' };
  }

  static getDerivedStateFromError(error) {
    // Actualiza el estado para que el siguiente renderizado muestre la interfaz de repuesto
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    // También puedes registrar el errorInfo en un servicio de reporte de errores
    logErrorToMyService(error, errorInfo);
    this.setState({ info: error });
  }

  render() {
    if (this.state.hasError) {
      // Puedes renderizar cualquier interfaz de repuesto
      return <h1>'Ups! Ha ocurrido un error...</h1>;
    }

    return this.props.children;
  }
}

export default ErrorBoundaires;
