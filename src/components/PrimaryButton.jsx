import React from 'react';
import Button from '@material-ui/core/Button';

const PrimaryButton = props => (
  <Button variant="contained" size="large" color="primary" {...props}>
    {props.children}
  </Button>
);

export default PrimaryButton;
