import React from 'react';
import { Button } from '@material-ui/core';

const NotFoundPage = ({ history: { replace } }) => (
  <>
    <h1>404</h1>
    <p>Página no encontrada</p>
    <Button variant="contained" color="secondary" onClick={() => replace('/')}>
      ir a la página principal
    </Button>
  </>
);

export default NotFoundPage;
