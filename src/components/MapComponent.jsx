import React, { useEffect, useState } from 'react';
import { compose, withProps, withStateHandlers } from 'recompose';
import { Circle, Polygon } from 'react-google-maps';
import InfoWindow from './InfoWindow';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from 'react-google-maps';
import MarkerClusterer from './CustomMarkerClusterer';
import mapConfig from '../mapConfig';
import mapHandlers from '../mapHandlers';
import { findIndexByProp } from '../utils/arrays';
import { StopMapButton } from './styles';

let counter = 0.0001;
const DEFAULT_MAP_ZOOM = 18;

function spliceItem(points, newPoint, newRange) {
  const pointIndex = findIndexByProp(points, 'id', newPoint.id);

  points.splice(pointIndex, 1, {
    ...newPoint,
    range: newRange
  });
}

const CampaignMapView = compose(
  withProps(mapConfig),
  withStateHandlers(() => ({ isOpen: false }), mapHandlers),
  withScriptjs,
  withGoogleMap
)(({ isMarkerShown, location, onMarkerClustererClick, onDblClick, clearWatching, watchPosition, watching, i18n }) => {
  const [points, setPoints] = useState([]);
  const [points2, setPoints2] = useState([]);
  const [points3, setPoints3] = useState([]);
  const [selectedMarker, setSelectedMarker] = useState(null);

  useEffect(() => {
    const point = {
      lat: location.lat + counter,
      lng: location.lng + counter,
      id: `index-${counter}`,
      range: 10
    };
    const point2 = {
      lat: location.lat - counter,
      lng: location.lng + counter,
      id: `index2-${counter}`,
      range: 10
    };
    const point3 = {
      lat: location.lat - counter,
      lng: location.lng - counter,
      id: `index3-${counter}`,
      range: 10
    };
    setPoints(p => p.concat(point));
    setPoints2(p => p.concat(point2));
    setPoints3(p => p.concat(point3));
    counter += 0.0001;
  }, [location]);

  const handleMarkerChange = (newPoint, newRange) => {
    const point = points.find(p => p.id === newPoint.id);
    if (point) {
      spliceItem(points, newPoint, newRange);
      setPoints([...points]);
    } else {
      const point2 = points2.find(p => p.id === newPoint.id);
      if (point2) {
        spliceItem(points2, newPoint, newRange);
        setPoints2([...points2]);
      } else {
        const point3 = points3.find(p => p.id === newPoint.id);
        if (point3) {
          spliceItem(points3, newPoint, newRange);
          setPoints3([...points3]);
        }
      }
    }
  };

  const renderCircle = point => <Circle key={point.id} radius={point.range} center={point} visible />;

  const renderMarker = locs => (
    <MarkerClusterer
      points={locs}
      onChange={handleMarkerChange}
      onMarkerClustererClick={onMarkerClustererClick}
      setSelectedMarker={setSelectedMarker}
      selectedMarker={selectedMarker}
    />
  );

  if (location.lat) {
    return (
      <GoogleMap defaultZoom={DEFAULT_MAP_ZOOM} center={location}>
        {isMarkerShown && <Marker position={location} onDblClick={onDblClick} />}
        <InfoWindow i18n={i18n} location={location} />
        {watching ? (
          <StopMapButton variant="contained" color="primary" onClick={clearWatching}>
            {i18n.get('app.map.stopSearch')}
          </StopMapButton>
        ) : (
          <StopMapButton variant="contained" color="secondary" onClick={watchPosition}>
            {i18n.get('app.map.startSearch')}
          </StopMapButton>
        )}
        {points.length > 0 && points.map(renderCircle)}
        {points2.length > 0 && points2.map(renderCircle)}
        {points3.length > 0 && points3.map(renderCircle)}

        <Polygon paths={points} options={{ strokeColor: '#FF0000' }} />
        <Polygon paths={points2} options={{ strokeColor: '#FF0000' }} />
        <Polygon paths={points3} options={{ strokeColor: '#FF0000' }} />

        {renderMarker(points)}
        {renderMarker(points2)}
        {renderMarker(points3)}
      </GoogleMap>
    );
  }
  return <div>Loading...</div>;
});

export default CampaignMapView;
