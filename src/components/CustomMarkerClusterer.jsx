import React, { useState } from 'react';
import { MarkerClusterer } from 'react-google-maps/lib/components/addons/MarkerClusterer';
import { Marker } from 'react-google-maps';
import { InfoWindow } from 'react-google-maps';

const CustomMarkerClusterer = ({ onMarkerClustererClick, onChange, points, setSelectedMarker, selectedMarker }) => {
  const [visible, setVisible] = useState(false);

  let changing;
  const handleDistance = point => event => {
    if (!changing) {
      onChange(point, Number(event.target.value));
    }
    changing = true;
    setTimeout(() => {
      changing = false;
    }, 300);
  };

  const handleMarkerClick = id => event => {
    setSelectedMarker(id);
    setVisible(true);
  };

  const handleCloseClick = () => {
    setSelectedMarker(null);
    setVisible(false);
  };

  return (
    <div>
      <MarkerClusterer onClick={onMarkerClustererClick} averageCenter enableRetinaIcons gridSize={20}>
        {points.map(point => (
          <div key={point.id}>
            <Marker position={{ lat: point.lat, lng: point.lng }} onClick={handleMarkerClick(point.id)} />
            {visible && selectedMarker === point.id && (
              <InfoWindow
                onCloseClick={handleCloseClick}
                defaultPosition={{ lat: point.lat, lng: point.lng }}
                options={{
                  closeBoxURL: ``,
                  enableEventPropagation: true,
                  closeBoxMargin: '12px 4px 2px 2px'
                }}
              >
                <div>
                  <p>Distancia de visión</p>
                  <p className="point-range__number">
                    <b>{point.range}</b>
                  </p>
                  <p>metros</p>
                  <input
                    type="range"
                    id="see"
                    name="see"
                    defaultValue={point.range}
                    min="1"
                    max="50"
                    onChange={handleDistance(point)}
                  />
                </div>
              </InfoWindow>
            )}
          </div>
        ))}
      </MarkerClusterer>
    </div>
  );
};

export default CustomMarkerClusterer;
