import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { actions } from '../../reducers/modal';

import MODAL_COMPONENTS from './modal-components';

const ModalRoot = ({ modalType, modalProps, show, closeModal }) => {
  const SpecificModal = MODAL_COMPONENTS[modalType];

  return (
    modalType && SpecificModal && <SpecificModal show={show} close={closeModal} {...modalProps} />
  );
};

ModalRoot.propTypes = {
  modalType: PropTypes.string,
  modalProps: PropTypes.object,
  show: PropTypes.bool.isRequired
};

const mapStateToProps = ({ modal }) => ({
  modalType: modal.modalType,
  modalProps: modal.modalProps,
  show: modal.show
});

const mapDispatchToProps = { ...actions };

export default connect(mapStateToProps, mapDispatchToProps)(ModalRoot);
