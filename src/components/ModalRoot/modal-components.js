import chatModals from '../../routes/Chat/modals';
import loginModals from '../../routes/Login/modals';
import teamModals from '../../routes/Teams/components/Team/modals';

export default {
  ...chatModals,
  ...loginModals,
  ...teamModals
};
