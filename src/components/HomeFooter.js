import React from 'react';
import { FooterStyled } from './styles';

const Footer = () => (
  <FooterStyled>
    <a href="/" alt="GBPD" target="_self">
      @GBPD {new Date().getFullYear()}
    </a>
  </FooterStyled>
);

export default Footer;
