import React from 'react';
import '../styles/campaigns.css';

const Campaign = ({ campaign }) => {
  const handleClickCampaign = () => {
    window.location.href = `/campaign/${campaign.id}`;
  };

  return (
    <div className="campaign-container" onClick={handleClickCampaign}>
      <div className="campaign-avatar">
        <img src="" width={100} height={100} alt="" />
      </div>
      <div className="campaign-content">
        <h2>{campaign.name}</h2>
        <p>Calle Lopez Maria, 32, Sagunto (Valencia)</p>
        <p>
          Desaparecida hace <strong>37</strong> horas
        </p>
      </div>
      <div className="campaign-indicators">
        <p>A</p>
        <p>B</p>
        <p>C</p>
      </div>
    </div>
  );
};

export default Campaign;
