import React from 'react';
import Card from '@material-ui/core/Card';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  root: {
    width: '100%',
    padding: '1rem'
  }
});

const Form = ({ children, classes }) => <Card className={classes.root}>{children}</Card>;

export default withStyles(styles)(Form);
