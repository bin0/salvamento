import React from 'react';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import { Button, TextField, CardActions, FormGroup, Typography } from '@material-ui/core';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { validateInput } from '../../utils/validations';
import { RegisterContainerStyled } from '../styles';

function reducer(state, action) {
  return { ...state, ...action };
}

const EmailPassword = ({ createNewUser, classes, isAuthenticating, location: { state = {} }, i18n }) => {
  const { email = '', password = '' } = state;
  const defaultState = { email: '', password1: '', password2: '' };
  const [form, setForm] = React.useReducer(reducer, defaultState);
  const [inputError, setInputError] = React.useReducer(reducer, defaultState);
  const [showPassword, setShowPassword] = React.useState(false);

  React.useEffect(() => {
    setForm({ email, password1: password, password2: password });
  }, [email, password]);

  function handleChange({ target }) {
    const { id, value } = target;
    setForm({ [id]: value });
  }

  function handleClick() {
    const { password1, password2, email } = form;
    if (!isEmpty(password1)) {
      if (isEqual(password1, password2)) {
        createNewUser({ email, password: password1 })
          .then(() => {
            setForm(defaultState);
          })
          .catch(() => {
            setInputError({ email: i18n.get('emailExists') });
          });
      } else {
        setInputError({
          password1: i18n.get('passwordNotMatch')
        });
        // Debe tener 6 carácteres y, al menos, 1 mayúscula, 1 minúscula y 1 número.
      }
    }
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleBlurEmail = () => {
    const errorText = !validateInput('email', form.email) ? `Introduce un email válido` : '';
    setInputError({ email: errorText });
  };
  const handleBlurPassword = () => {
    const { password1: pas1, password2: pas2 } = form;
    const match = validateInput('password', pas1) && pas1 === pas2;
    const errorText = !match ? 'La contraseña no es válida' : '';
    setInputError({ password1: errorText });
  };

  return (
    <RegisterContainerStyled>
      <CardContent>
        <TextField
          className={classes.textField}
          error={inputError.email !== ''}
          fullWidth
          helperText={inputError.email}
          id="email"
          margin="dense"
          name="email"
          onBlur={handleBlurEmail}
          onChange={handleChange}
          placeholder={i18n.get('email')}
          required
          type="email"
          variant="standard"
          value={form.email}
        />
        <FormGroup>
          <TextField
            className={classes.textPassword}
            color="primary"
            error={inputError.password1 !== ''}
            helperText={inputError.password1}
            id="password1"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
            type={showPassword ? 'text' : 'password'}
            margin="dense"
            name="password"
            onBlur={handleBlurPassword}
            onChange={handleChange}
            placeholder="password"
            required
            value={form.password1}
          />
          <TextField
            className={classes.textPassword}
            color="primary"
            error={inputError.password2 !== ''}
            helperText={inputError.password2}
            id="password2"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                  >
                    {showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
            margin="dense"
            name="password"
            onBlur={handleBlurPassword}
            onChange={handleChange}
            placeholder="repeat password"
            required
            type={showPassword ? 'text' : 'password'}
            value={form.password2}
          />
        </FormGroup>
        <div style={{ marginTop: '1rem', lineHeight: '.5rem' }}>
          <Typography variant="caption">
            La contraseña debe tener al menos 6 caracteres, mayúsuculas, minúsculas y números
          </Typography>
        </div>
      </CardContent>
      <CardActions>
        <Button disabled={isAuthenticating} variant="contained" color="secondary" onClick={handleClick}>
          {i18n.get('createAccount')}
        </Button>
      </CardActions>
    </RegisterContainerStyled>
  );
};

export default EmailPassword;
