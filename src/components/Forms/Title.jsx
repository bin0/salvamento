import React from 'react';
import { Typography } from '@material-ui/core';

export default ({ children }) => <Typography variant='h6'>{children}</Typography>;
