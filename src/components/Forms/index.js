import Forms from './Form';
import Input from './Input';
import Title from './Title';

Forms.Input = Input;
Forms.Title = Title;

export default Forms;
