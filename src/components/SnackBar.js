import React from 'react';
import { connect } from 'react-redux';
import { Snackbar, IconButton, Slide, withStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { actions } from '../reducers/snack';

function TransitionUp(props) {
  return <Slide {...props} direction="up" />;
}

const styles = theme => ({
  close: {
    padding: theme.spacing(2)
  }
});

class SnackBar extends React.Component {
  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.props.closeSnack();
  };
  render() {
    const { classes, snackOpen, snackMessage } = this.props;
    return (
      <Snackbar
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className={classes.close}
            onClick={this.handleClose}
          >
            <CloseIcon />
          </IconButton>
        ]}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        autoHideDuration={5000}
        ContentProps={{
          'aria-describedby': 'snack-message'
        }}
        onClose={this.handleClose}
        open={snackOpen}
        message={<span id="snack-message">{snackMessage}</span>}
        TransitionComponent={TransitionUp}
      />
    );
  }
}

const mapStateToProps = ({ snack }) => ({
  snackOpen: snack.open,
  snackMessage: snack.message
});

const mapDispatchToProps = { ...actions };

const StyledSnackBar = withStyles(styles)(SnackBar);

export default connect(mapStateToProps, mapDispatchToProps)(StyledSnackBar);
