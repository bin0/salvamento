export default {
  useNextVariants: true,
  fontFamily: [
    'Rubik',
    'sans-serif',
    '-apple-system',
    'BlinkMacSystemFont',
    '"Segoe UI"',
    '"Helvetica Neue"',
    'Arial',
    'sans-serif',
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"'
  ].join(','),
  body1: {
    fontSize: '16px',
    fontSmoothing: 'auto',
    fontWeight: 300
  },
  subtitle1: {
    lineHeight: '1.2rem',
    letterSpacing: '-0.027rem',
    fontWeight: 300
  },
  subtitle2: {
    fontFamily: 'Rubik-Regular, sans-serif',
    '-webkit-font-smoothing': 'antialiased'
  },
  body2: {
    fontFamily: 'Rubik, sans-serif',
    fontSize: '16px',
    fontWeight: 'bold'
  },
  caption: {
    lineHeight: '1rem'
  },
  h1: {
    fontSize: '2rem'
  },
  h2: {
    fontFamily: 'Rubik, sans-serif',
    fontSize: '1.5rem'
  },
  h3: {
    fontFamily: 'Rubik, sans-serif',
    fontSize: '16px'
  },
  h4: {},
  h5: {
    fontSize: '28px',
    fontWeight: 300
  },
  h6: {
    fontFamily:
      'Rubik,sans-serif,-apple-system,BlinkMacSystemFont,"Segoe UI","Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"'
  }
  // textSecondary: {
  //   color: '#fff'
  // }
};
