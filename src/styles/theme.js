import { createMuiTheme } from '@material-ui/core/styles';
import overrides from './overrides';
import typography from './typography';
import palette from './palette';

export default createMuiTheme({
  overrides,
  typography,
  palette
});
