import { green } from '@material-ui/core/colors';
const primaryColor = '#8ee7ff';
const secondaryColor = '#282c33';

export default {
  // type: 'dark',
  action: {
    main: green[600]
  },
  primary: {
    main: primaryColor,
    contrastText: secondaryColor
  },
  textSecondary: {
    main: '#fff',
    color: '#fff'
  },
  text: {
    main: secondaryColor,
    bull: '#07ac00',
    bear: '#ff0000'
  },
  colorTextSecondary: {
    color: '#fff'
  },
  secondary: {
    main: secondaryColor,
    contrastText: primaryColor
  },
  common: {
    black: secondaryColor,
    gray: primaryColor,
    white: primaryColor
  }
};
