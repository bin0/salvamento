const primaryColor = '#8ee7ff';
const secondaryColor = '#282c33';

export default {
  MuiAppBar: {
    root: {
      background: secondaryColor
    },
    positionSticky: {
      background: secondaryColor
    },
    colorDefault: {
      background: secondaryColor
    },
    colorPrimary: {
      background: secondaryColor
    },
    colorSecondary: {
      background: secondaryColor
    }
  },
  MuiAvatar: {
    root: {
      cursor: 'pointer',
      '&:hover': {
        opacity: 0.6,
        '&::after': {
          content: 'A',
          position: 'relative',
          top: '10px',
          left: '10px',
          zIndex: 10000
        }
      },
      background: primaryColor
    }
  },
  MuiButton: {
    // root: {
    //   color: secondaryColor,
    //   background: primaryColor,
    //   '&:hover': {
    //     backgroundColor: '#D8604B',
    //     color: '#fff'
    //   },
    //   '&$disabled': {
    //     borderColor: 'white',
    //     color: 'white'
    //   }
    // }
  },
  MuiCard: {
    root: {
      padding: '1rem',
      display: 'flex',
      backgroundColor: primaryColor,
      flexDirection: 'column',
      color: secondaryColor,
      textAlign: 'left'
    }
  },
  MuiCardActions: {
    root: {
      padding: '16px'
    }
  },
  MuiCardContent: {
    root: {
      width: '100%'
    }
  },
  MuiFormGroup: {
    root: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'nowrap',
      justifyContent: 'space-between'
    }
  },
  MuiSlider: {
    root: {
      '&$disabled': {
        color: 'grey'
      },
      color: primaryColor
    },
    active: {
      border: '1px solid #282c33',
      background: primaryColor
    },
    colorPrimary: {
      // background: primaryColor,
      // color: primaryColor,
    },
    thumbColorPrimary: {
      color: secondaryColor
    },
    mark: {
      color: secondaryColor
    },
    marked: {
      color: secondaryColor
    },
    markLabel: {
      color: primaryColor
    },
    markLabelActive: {
      color: secondaryColor
    }
  },
  MuiListItemText: {
    root: {
      margin: 0,
      padding: 0,
      lineHeight: 0
    }
  },
  MuiTextField: {
    root: {}
  },
  MuiInputBase: {
    root: {}
  },
  MuiSnackbar: {}
};
