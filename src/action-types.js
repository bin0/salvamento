function requestAction(action) {
  return {
    REQUEST: `${action}_REQUEST`,
    SET: `${action}_SET`,
    SUCCESS: `${action}_SUCCESS`,
    FAILURE: `${action}_FAILURE`,
    UPDATE: `${action}_UPDATE`
  };
}

// MODALS
export const OPEN_MODAL = requestAction('/modals/OPEN_MODAL');
export const CLOSE_MODAL = requestAction('/modals/CLOSE_MODAL');
export const OPEN_DRAWER = requestAction('/modals/OPEN_DRAWER');
export const CLOSE_DRAWER = requestAction('/modals/CLOSE_DRAWER');

// SNACKS
export const CLOSE_SNACK = requestAction('/modals/CLOSE_SNACK');
export const OPEN_SNACK = requestAction('/modals/OPEN_SNACK');

export const UPDATE_USER_AVATAR = requestAction('/modals/UPDATE_USER_AVATAR');
export const UPDATE_USER_OPTIONS = requestAction('/modals/UPDATE_USER_OPTIONS');

// USER
export const DELETE_USER = requestAction('/user/DELETE_USER');
export const LOG_OUT = requestAction('/user/LOG_OUT');
export const LOG_IN = requestAction('/user/LOG_IN');
export const SET_USER_NAME = requestAction('/user/SET_USER_NAME');
export const CHECK_USER = requestAction('/user/CHECK_USER');
export const GET_USER = requestAction('/user/GET_USER');
export const SET_USER = requestAction('/user/SET_USER');
export const CREATE_NEW_USER = requestAction('/user/CREATE_NEW_USER');
export const SET_USER_LOCATION = requestAction('user/SET_USER_LOCATION');
export const GET_USER_LOCATION = requestAction('user/GET_USER_LOCATION');
export const GET_USERS_LOCATION = requestAction('user/GET_USERS_LOCATION');
export const GET_TEAM_USER = requestAction('user/GET_TEAM_USER');

export const SET_USER_LANGUAGE = requestAction('user/SET_USER_LANGUAGE');
export const SET_USER_SELL_AMOUNT = requestAction('user/SET_USER_SELL_AMOUNT');
export const SET_AUTHENTICATED = requestAction('user/SET_AUTHENTICATED');
export const SET_AUTHENTICATING = requestAction('user/SET_AUTHENTICATING');
export const GET_REMOTE_USER = requestAction('user/GET_REMOTE_USER');
export const SET_USER_ADDRESS = requestAction('user/SET_USER_ADDRESS');

export const VALIDATE_USER_TOKEN = requestAction('auth/VALIDATE_USER_TOKEN');

export const INIT_I18N = requestAction('i18n/INIT_I18N');

export const GET_USERS_CONVERSATION = requestAction('chat/GET_USERS_CONVERSATION');
export const GET_USER_CHATS = requestAction('chat/GET_USER_CHATS');
export const GET_CHAT_MESSAGES = requestAction('chat/GET_CHAT_MESSAGES');
export const SET_NEW_CHAT_MESSAGE = requestAction('chat/SET_NEW_CHAT_MESSAGE');
export const SET_CHAT_MESSAGES = requestAction('chat/SET_CHAT_MESSAGES');

export const SET_SELECTED_DISTANCE = requestAction('settings/SET_SELECTED_DISTANCE');
export const GET_USER_SETTINGS = requestAction('settings/GET_USER_SETTINGS');

export const REQUEST_PASSWORD = requestAction('login/REQUEST_PASSWORD');
export const SET_UNLOCK_CODE = requestAction('login/SET_UNLOCK_CODE');

export const SET_USER_CONNECTED_COUNT = requestAction('socket/SET_USER_CONNECTED_COUNT');
export const SET_CONNECTED_USER = requestAction('socket/SET_CONNECTED_USER');

// ALERTS
export const SET_ALERTS = requestAction('alerts/SET_ALERTS');

// TEAMS
export const SET_TEAMS = requestAction('teams/SET_TEAMS');
export const SET_TEAM = requestAction('teams/SET_TEAM');

// TEAM
export const SET_TEAM_CHAT_MESSAGES = requestAction('team/SET_TEAM_CHAT_MESSAGES');
