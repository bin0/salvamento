import axios from 'axios';
import history from '../redux/history';

const localApiDevUrls = {
  local: `${process.env.REACT_APP_API_URL}/${process.env.REACT_APP_API_VERSION}`,
  dev: `http://localhost:3009/${process.env.REACT_APP_API_VERSION}`
};

const URL = localApiDevUrls[process.env.REACT_APP_DEV_ENV];

const axiosInstance = axios.create({
  baseURL: URL || localApiDevUrls.local,
  withCredentials: true,
  headers: {
    'Access-Control-Allow-Origin': process.env.REACT_APP_PUBLIC_URL
  }
});

axiosInstance.interceptors.response.use(
  response => {
    // console.log('cookies', response.cookies);
    return response;
  },
  function(error) {
    if (error.response.status === 401) {
      if (error.response.data.code !== 'auth/user-not-found') {
        history.push('/login');
      }
    }
    if (error.response.data.code === 1003) {
      localStorage.removeItem('wc-token');
      history.push('/login');
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
