import { setUserLanguage } from '../reducers/user/local';
import { setI18n } from '../reducers/i18n';

export default store => {
  const { language = 'es' } = window.navigator;

  store.dispatch(setUserLanguage(language));
  store.dispatch(setI18n(language));
};
