import * as Sentry from '@sentry/browser';
import { name, version } from '../../package.json';

if (process.env.NODE_ENV === 'production') {
  Sentry.init({
    dsn: process.env.REACT_APP_SENTRY_DSN,
    environment: process.env.NODE_ENV,
    release: `${name}@${version}`
  });
}
