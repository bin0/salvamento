// Google Analytics integration
import ReactGA from 'react-ga';

(function initializeReactGA() {
  let userId = localStorage.getItem('userId');
  if (!userId) {
    userId = 'Invitado';
  }
  if (process.env.NODE_ENV === 'development') {
    // ReactGA.initialize(process.env.REACT_APP_GA_ID, {
    //   debug: true,
    //   gaOptions: { userId },
    // });
  } else {
    ReactGA.initialize(process.env.REACT_APP_GA_ID, { gaOptions: { userId } });
  }
})();
