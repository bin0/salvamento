import React from 'react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import App from './App';
import createStore from '../redux/create-store';
import theme from '../styles/theme';
import initLanguage from '../config/language';

const store = createStore();
initLanguage(store);

test('renders home page', () => {
  const { queryAllByText } = render(
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <App />
      </Provider>
    </MuiThemeProvider>
  );

  const linkElement = queryAllByText(/Register/i);
  expect(linkElement).toHaveLength(1);
});
