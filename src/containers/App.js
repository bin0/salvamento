import React, { Suspense } from 'react';
import { Router, Switch } from 'react-router-dom';
import LoadingBar from 'react-redux-loading-bar';
import history from '../redux/history';
import ErrorBoundaires from '../components/ErrorBoundaries';
import routesBoot from '../routes/routes';

import './App.scss';

const App = () => (
  <Router history={history}>
    <ErrorBoundaires>
      <Suspense fallback={<LoadingBar className="redux-loading-bar" showFastActions />}>
        <Switch>{routesBoot()}</Switch>
      </Suspense>
    </ErrorBoundaires>
  </Router>
);

export default App;
