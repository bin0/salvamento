import { combineReducers } from 'redux';
import local from './local';
import location from './location';
import settings from './settings';

export default combineReducers({ local, location, settings });
