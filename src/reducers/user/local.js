import createReducer from '../../redux/create-reducer';
import axios from '../../config/axios';
import { openSnack } from '../snack';
import * as ACTIONS from '../../action-types';
import * as authActions from '../auth';

/**
 *
 * @param {object} user
 *
 * @dispatch setUser()
 * @throws {error} error
 */
export const createNewUser = user => async dispatch => {
  dispatch(authActions.setAuthenticating(true));
  dispatch({ type: ACTIONS.CREATE_NEW_USER.REQUEST });

  try {
    const url = 'user/new';
    const { data } = await axios.post(url, user);

    dispatch(authActions.logIn(user.email, user.password));
    dispatch(openSnack(`Bienvenido ${data.displayName}`));
    dispatch({ type: ACTIONS.CREATE_NEW_USER.SUCCESS });
  } catch (error) {
    dispatch({ type: ACTIONS.CREATE_NEW_USER.FAILURE });
  } finally {
    return true;
  }
};

export const deleteUser = () => dispatch => {
  dispatch({ type: ACTIONS.DELETE_USER.SET });
};

export const getUser = () => async dispatch => {
  dispatch({ type: ACTIONS.GET_USER.REQUEST });

  try {
    const URL = 'user';
    const { data } = await axios(URL);

    dispatch(setUser(data));
    dispatch(authActions.setAuthenticated());
    dispatch({ type: ACTIONS.GET_USER.SUCCESS });
  } catch (err) {
    console.error(err);
    dispatch({ type: ACTIONS.GET_USER.FAILURE });
  }
};

export const setUser = user => dispatch => {
  dispatch({ type: ACTIONS.SET_USER.SET, payload: user });
};

const setUserName = username => dispatch => {
  dispatch({ type: ACTIONS.SET_USER_NAME.SET, payload: username });
};

export const setUserLanguage = (language = 'es') => dispatch => {
  dispatch({ type: ACTIONS.SET_USER_LANGUAGE.SET, payload: language });
};

export const validateUserToken = token => async dispatch => {
  dispatch({ type: ACTIONS.VALIDATE_USER_TOKEN.REQUEST });
  try {
    axios.defaults.headers.Authorization = `Bearer ${token}`;
    const URL = 'login/validate-token';
    const { data: newToken } = await axios.post(URL);

    dispatch(authActions.refreshUserToken(newToken));
    dispatch(getUser());
    dispatch({ type: ACTIONS.VALIDATE_USER_TOKEN.SUCCESS });
  } catch (error) {
    dispatch(authActions.logOut());
    dispatch({ type: ACTIONS.VALIDATE_USER_TOKEN.FAILURE });
  }
};

export const actions = {
  getUser,
  setUser,
  setUserName,
  createNewUser
};

const INITIAL_STATE = {
  role: 'user',
  displayName: 'Invitado',
  email: '',
  uid: '',
  _id: '',
  language: 'es-ES',
  isOnline: false,
  avatar: {},
  permissions: ['user']
};

const ACTION_HANDLER = {
  [ACTIONS.SET_USER.SET]: (state, { payload }) => ({
    ...state,
    ...payload
  }),
  [ACTIONS.SET_USER.FAILURE]: state => ({ ...state, ...INITIAL_STATE }),
  [ACTIONS.SET_USER_NAME.SET]: (state, { payload }) => ({
    ...state,
    username: payload
  }),
  [ACTIONS.SET_USER_LANGUAGE.SET]: (state, { payload }) => ({
    ...state,
    language: payload
  }),
  [ACTIONS.VALIDATE_USER_TOKEN.SUCCESS]: state => ({
    ...state,
    isOnline: true
  }),
  [ACTIONS.VALIDATE_USER_TOKEN.FAILURE]: state => ({
    ...state,
    isOnline: false
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
