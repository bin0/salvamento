import createReducer from "../../redux/create-reducer";
import axios from "../../config/axios";
import {
  // GET_USER_LOCATION,
  SET_USER_LOCATION,
  GET_USERS_LOCATION,
  SET_USER_ADDRESS
} from "../../action-types";
import { openSnack } from "../../reducers/snack";

export const getUserDeviceLocation = () => dispatch => {
  function success(pos) {
    localStorage.setItem("wc-locationAllowed", 1);
    dispatch(setUserLocation(pos));
  }

  function error(err) {
    dispatch(openSnack("No se ha podido obtener la ubicación del dispositivo"));
  }

  const options = {
    timeout: 30000,
    maximumAge: 25000
  };

  navigator.geolocation.getCurrentPosition(success, error, options);
};

export const getUsersLocation = () => async dispatch => {
  dispatch({ type: GET_USERS_LOCATION.REQUEST });

  try {
    const URL = "locations";
    const { data } = await axios(URL);

    // console.log('getUsersLocation data', data);

    dispatch({ type: GET_USERS_LOCATION.SET, payload: data });
    dispatch({ type: GET_USERS_LOCATION.SUCCESS });
  } catch (error) {
    console.error("error =>", error);
    dispatch({ type: GET_USERS_LOCATION.FAILURE });
  }
};

const updateUserLocation = async position => {
  try {
    const url = "location";
    await axios.post(url, position);

    return true;
  } catch (error) {
    throw new Error("Error al crear la ubicación del usuario");
  }
};

export const setUserLocation = pos => async dispatch => {
  if (!pos) {
    throw new Error("Missing `position`");
  }

  try {
    const position = {
      coords: {
        latitude: pos.coords.latitude,
        longitude: pos.coords.longitude
      }
    };
    dispatch({
      type: SET_USER_LOCATION.SET,
      payload: position
    });
    dispatch(setUserAddressFromLocation(position.coords));
    await updateUserLocation(position);
  } catch (error) {
    console.error("[setUserLocation] ERROR =>", error);
    dispatch({
      type: SET_USER_LOCATION.FAILURE
    });
  }
};

let counter = 0;
export const setUserAddressFromLocation = coords => async dispatch => {
  dispatch({ type: SET_USER_ADDRESS.REQUEST });

  try {
    if (!coords) {
      throw new Error("No tengo coordenadas");
    }
    if (counter > 1) {
      throw new Error("Demasiados intentos");
    }

    const URL = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords.latitude},${coords.longitude}&key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}`;
    fetch(URL)
      .then(res => res.json())
      .then(data => {
        const formattedAddress = data.results[0].formatted_address || "";
        dispatch({
          type: SET_USER_ADDRESS.SET,
          payload: formattedAddress
        });
        dispatch({ type: SET_USER_ADDRESS.SUCCESS });
      });
  } catch (err) {
    console.error("Error =>", err);
    dispatch({ type: SET_USER_ADDRESS.FAILURE });
  }
};

export const actions = {
  // getUserLocation,
  getUserDeviceLocation,
  getUsersLocation,
  setUserLocation,
  setUserAddressFromLocation
};

const INITIAL_STATE = {
  position: {
    coords: {}
  },
  destinations: [],
  address: ""
};

const ACTION_HANDLER = {
  [SET_USER_LOCATION.SET]: (state, { payload }) => {
    return {
      ...state,
      position: { ...payload }
    };
  },
  [GET_USERS_LOCATION.SET]: (state, { payload }) => ({
    ...state,
    destinations: payload
  }),
  [SET_USER_ADDRESS.SET]: (state, { payload }) => ({
    ...state,
    address: payload
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
