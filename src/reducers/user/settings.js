import createReducer from "../../redux/create-reducer";
import axios from "../../config/axios";
import { SET_SELECTED_DISTANCE, GET_USER_SETTINGS } from "../../action-types";
import { getUsersLocation } from "./location";

export const getUserSettings = () => async dispatch => {
  dispatch({ type: GET_USER_SETTINGS.REQUEST });
  try {
    const URL = "settings";
    const { data } = await axios(URL);

    dispatch(setUserSettings(data));
    dispatch({ type: GET_USER_SETTINGS.SUCCESS });
  } catch (error) {
    dispatch({ type: GET_USER_SETTINGS.FAILURE });
  }
};

export const setUserSettings = settings => dispatch => {
  if (!settings) {
    throw new Error("Missing `settings`");
  }
  dispatch({ type: GET_USER_SETTINGS.SET, payload: settings });
};

const setSelectedDistance = distance => async dispatch => {
  dispatch({ type: SET_SELECTED_DISTANCE.REQUEST });
  try {
    const URL = "settings/distance";
    await axios.put(URL, { distance });

    dispatch({ type: SET_SELECTED_DISTANCE.SET, payload: distance });
    dispatch({ type: SET_SELECTED_DISTANCE.SUCCESS });
    dispatch(getUsersLocation());
  } catch (error) {
    dispatch({ type: SET_SELECTED_DISTANCE.FAILURE });
    console.error(error);
  }
};

export const actions = {
  setSelectedDistance,
  getUserSettings
};

const INITIAL_STATE = {
  selectedDistance: 50
};

const ACTION_HANDLER = {
  [SET_SELECTED_DISTANCE.SET]: (state, { payload }) => ({
    ...state,
    selectedDistance: payload
  }),
  [GET_USER_SETTINGS.SET]: (state, { payload }) => ({
    ...state,
    ...payload
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
