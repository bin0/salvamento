import createReducer from '../redux/create-reducer';
// import axios from '../config/axios';

export const actions = {};

export const INITIAL_STATE = {
  data: [],
  isLoading: false,
  error: ''
};

export const ACTION_HANDLERS = {};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
