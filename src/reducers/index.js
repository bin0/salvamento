import { combineReducers } from 'redux';

import alerts from '../routes/Alerts/modules/alerts';
import auth from './auth';
// import chat from './chat';
// import { reducer as chatsReducer } from '../routes/Chats';
import i18n from './i18n';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar';
import { reducer as loginReducer } from '../routes/Login';
import modal from './modal';
import snack from './snack';
import user from './user';
import users from './users';
import team from '../routes/Teams/components/Team/modules/team';
import teamChat from '../routes/Teams/components/Team/components/TeamChat/modules/team-chat';
import { reducer as teamUserReducer } from '../routes/User';
import teams from '../routes/Teams/modules/teams';

export default combineReducers({
  alerts,
  auth,
  // chat,
  // chats: chatsReducer,
  i18n,
  loadingBar,
  login: loginReducer,
  modal,
  snack,
  user,
  users,
  team,
  teamChat,
  teamUser: teamUserReducer,
  teams
});
