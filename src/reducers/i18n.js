import createReducer from '../redux/create-reducer';
import i18n from '../i18n';
import { INIT_I18N } from '../action-types';

export const setI18n = language => dispatch => {
  dispatch({ type: INIT_I18N.SET, payload: i18n[language] });
};

export const actions = {
  setI18n
};

const INITIAL_STATE = {
  literals: {},
  get: function get(id) {
    return this.literals[id] || `[i18n]! => ${id}`;
  }
};

const ACTION_HANDLER = {
  [INIT_I18N.SET]: (state, { payload }) => ({ ...state, literals: payload })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
