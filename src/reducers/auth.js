import axios from '../config/axios';
import createReducer from '../redux/create-reducer';
import history from '../redux/history';
import { LOG_IN, LOG_OUT, SET_AUTHENTICATING, SET_AUTHENTICATED, SET_USER, REQUEST_PASSWORD } from '../action-types';
import { openSnack } from './snack';

export const logOut = () => async (dispatch, getState) => {
  try {
    const uid = getState().user.local.uid;
    await axios(`/logout/${uid}`);
  } catch (err) {
    console.error(err);
  }
  axios.defaults.headers.Authorization = null;
  localStorage.removeItem('wc-token');

  dispatch({ type: SET_USER.FAILURE });
  dispatch({ type: LOG_OUT.SUCCESS });
  history.push('/login');
};

export const refreshUserToken = token => dispatch => {
  axios.defaults.headers.Authorization = `Bearer ${token}`;
  localStorage.setItem('wc-token', token);

  dispatch({ type: LOG_IN.SET, payload: token });
};

export const sendPasswordResetEmail = emailAddress => async dispatch => {
  dispatch({ type: REQUEST_PASSWORD.REQUEST });

  try {
    // TODO service in back missing
    const URL = 'login/reset-password';
    await axios.post(URL, { emailAddress });

    dispatch({ type: REQUEST_PASSWORD.SUCCESS });
  } catch (error) {
    console.error(error);
    dispatch({ type: REQUEST_PASSWORD.FAILURE });
  }
};

export const setAuthenticated = () => dispatch => {
  dispatch({ type: SET_AUTHENTICATED.SET });
};

export const setAuthenticating = value => dispatch => {
  dispatch({ type: SET_AUTHENTICATING.SET, payload: value });
};

export const logIn = (email, password) => async dispatch => {
  dispatch({ type: LOG_IN.REQUEST });

  try {
    const URL = 'login';
    const { data: token } = await axios.post(URL, { email, password });

    if (token) {
      dispatch(refreshUserToken(token));
      dispatch({ type: LOG_IN.SUCCESS });
      history.push('/dashboard');
    }
  } catch (error) {
    console.log('error', error);
    // if (error.response.data.code === 'auth/user-not-found') {
    //   history.push('/login/register', { email, password });
    // }
    dispatch(openSnack('Usuario o contraseña incorrectos.'));
    dispatch({ type: LOG_IN.FAILURE });
    dispatch(setAuthenticating(false));
  }
};

export const actions = {
  logOut,
  setAuthenticating,
  sendPasswordResetEmail,
  logIn
};

const INITIAL_STATE = {
  authenticated: false,
  isAuthenticating: false,
  isRegistering: false,
  isSigninIn: false,
  token: null
};

const ACTION_HANDLERS = {
  [LOG_IN.REQUEST]: state => ({ ...state, isAuthenticating: true }),
  [LOG_IN.SET]: (state, { payload }) => ({
    ...state,
    token: payload,
    isAuthenticating: false
  }),
  [SET_AUTHENTICATED.SET]: state => ({
    ...state,
    authenticated: true
  }),
  [LOG_OUT.SUCCESS]: () => INITIAL_STATE,
  [SET_AUTHENTICATING.SET]: (state, { payload }) => ({
    ...state,
    isAuthenticating: payload
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
