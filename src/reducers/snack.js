import createReducer from '../redux/create-reducer';
import { OPEN_SNACK, CLOSE_SNACK } from '../action-types';

const INITIAL_STATE = {
  open: false,
  message: ''
};

export const openSnack = message => dispatch => {
  dispatch({ type: OPEN_SNACK.SET, payload: message });
};
const closeSnack = () => dispatch => {
  dispatch({ type: CLOSE_SNACK.SET });
};

export const actions = {
  openSnack,
  closeSnack
};

const ACTION_HANDLER = {
  [CLOSE_SNACK.SET]: () => INITIAL_STATE,
  [OPEN_SNACK.SET]: (state, { payload }) => ({
    message: payload,
    open: true
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
