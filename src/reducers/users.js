import createReducer from '../redux/create-reducer';
import { SET_USER_CONNECTED_COUNT } from '../action-types';

export const setConnectedUsersCount = count => dispatch => {
  dispatch({ type: SET_USER_CONNECTED_COUNT.SET, payload: count });
};

export const actions = { setConnectedUsersCount };

const INITIAL_STATE = { count: 0 };

const ACTION_HANDLER = {
  [SET_USER_CONNECTED_COUNT.SET]: (_, { payload }) => ({
    count: payload
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLER);
