import React from 'react';
import ReactDOM from 'react-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import App from './containers/App';
import createStore from './redux/create-store';
import theme from './styles/theme';
import initLanguage from './config/language';
import './index.scss';
import './config/analitycs';
import './config/sentry';
import { openSnack } from './reducers/snack';
import { version } from '../package.json';

const { NODE_ENV, REACT_APP_ENV_NAME } = process.env;

/**
 * Auto Update window title (navigator)
 */
function setWindowTitle() {
  const title = ['GBPD', version];
  if (NODE_ENV === 'development' && REACT_APP_ENV_NAME) {
    title.unshift(`[${REACT_APP_ENV_NAME}]`);
  }

  document.getElementsByTagName('title')[0].innerHTML = title.join(' ');
}

const store = createStore();
initLanguage(store);
setWindowTitle();
store.dispatch(openSnack(`Estás viendo la versión: ${version}`));

const Root = () => (
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </MuiThemeProvider>
);

ReactDOM.render(<Root />, document.getElementById('root'));
