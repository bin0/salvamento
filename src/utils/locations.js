import axios from 'axios';
import locations from '../mocks/locations';

const mathPi = 0.017453292519943295;
const R = 12742;

export function getDistance(lat1, lon1, lat2, lon2) {
  var p = mathPi; // Math.PI / 180
  var c = Math.cos;
  var a = 0.5 - c((lat2 - lat1) * p) / 2 + (c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p))) / 2;

  return R * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

function transformOrigin({ coords }) {
  return encodeURI(`${coords.latitude},${coords.longitude}`);
}

function transforDestionations(dest) {
  const destinations = dest.map((d, i) => {
    if (i === dest.length - 1) {
      return `${d.latitude},${d.longitude}`;
    } else {
      return `${d.latitude},${d.longitude}|`;
    }
  });
  const join = destinations.join('');
  return encodeURI(join);
}

export const getDistances = async from => {
  const origin = transformOrigin(from);
  const destinations = transforDestionations(locations);

  try {
    const googleMapsEndpoint = `https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=${origin}&destinations=${destinations}&key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}`;

    const response = await axios('https://wallacoin.com/' + googleMapsEndpoint);
    console.log('response =>', response);
  } catch (error) {
    console.error(error);
  }
};
