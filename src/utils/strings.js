export const getFirstLetter = str => {
  if (!str) {
    return 'Invitado';
  }
  return str.charAt(0).toUpperCase();
};

export const capitalize = str => str.charAt(0) + str.slice(1);
