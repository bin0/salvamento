export const findIndexByProp = (array, prop, value) => array.map(a => a[prop]).indexOf(value);
