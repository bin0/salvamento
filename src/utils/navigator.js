export async function getNavigatorPermissions(name) {
  return navigator.permissions.query({ name });
}
