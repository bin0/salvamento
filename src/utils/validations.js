export const validateInput = (type, value) => {
  if (value !== '') {
    if (type === 'email') {
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(value).toLowerCase());
    }
    if (type === 'password') {
      const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/;
      // const regex = /\d{6,}/; // 6 DIGITOS
      return regex.test(String(value));
    }
  }
  return false;
};

export function simpleReducer(state, action) {
  return { ...state, ...action };
}
