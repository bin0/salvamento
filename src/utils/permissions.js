export const hasPermission = permissions => permission => permissions.some(p => p === permission);
