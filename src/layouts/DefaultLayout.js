import React from 'react';
import { LoadingBar } from 'react-redux-loading-bar';
import AppBar from '../components/AppBar';

const PortfolioLayout = ({ children }) => {
  return (
    <div>
      <LoadingBar className="redux-loading-bar" showFastActions />

      <AppBar color="primary" title="Adolfo Onrubia" subtitle="Portfolio" />

      {children}
    </div>
  );
};

export default PortfolioLayout;
