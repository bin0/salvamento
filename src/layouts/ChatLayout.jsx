import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { LoadingBar } from 'react-redux-loading-bar';
import ReactGA from '../modules/reactGa';
import SnackBar from '../components/SnackBar';
import AppBar from '../components/AppBar';
import Menu from '../components/Menu';
import HomeFooter from '../components/HomeFooter';
import { ChatLayoutContent, ChatLayoutContainer } from './styles';

class ChatLayout extends PureComponent {
  componentDidMount() {
    const {
      location: { pathname }
    } = this.props;
    ReactGA('pageview', pathname, []);
    ReactGA('event', {
      category: 'User',
      action: `Navigates to ${pathname}`,
      value: 1,
      label: 'Page Visited',
      nonInteraction: true
    });
  }

  render() {
    return (
      <ChatLayoutContainer>
        <AppBar
          location={this.props.location}
          history={this.props.history}
          color="primary"
          title="GBP"
          subtitle="Buscamos desparecid@s"
        />
        <LoadingBar className="redux-loading-bar" showFastActions />
        <ChatLayoutContent>
          <Menu />
          <SnackBar />

          {this.props.children}

          <HomeFooter />
        </ChatLayoutContent>
      </ChatLayoutContainer>
    );
  }
}

const mapStateToProps = ({ auth, user }) => ({
  authenticated: auth.authenticated
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(ChatLayout);
