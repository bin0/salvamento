import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import LoadingBar from 'react-redux-loading-bar';
import { sendPage } from '../modules/reactGa';
import SnackBar from '../components/SnackBar';
import AppBar from '../components/AppBar';

import Menu from '../components/Menu';
import HomeFooter from '../components/HomeFooter';
import { PublicLayoutStyled } from './styles';
import ModalRoot from '../components/ModalRoot';

const PublicLayout = ({ location, history, children }) => {
  useEffect(() => {
    sendPage(location.pathname);
  }, [location.pathname]);
  return (
    <React.Fragment>
      <AppBar
        pathname={location.pathname}
        push={history.push}
        color="secondary"
        title="GBPD"
        subtitle="Buscamos desaparecid@s"
      />
      <LoadingBar className="redux-loading-bar" showFastActions />
      <PublicLayoutStyled>
        <Menu />
        <SnackBar />
        <div className="App">
          <div className="App-header">{children}</div>
        </div>
        <HomeFooter />
      </PublicLayoutStyled>
      <ModalRoot />
    </React.Fragment>
  );
};

PublicLayout.propTypes = {
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired
};

export default PublicLayout;
