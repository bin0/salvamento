import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import ChatView from '../components/ChatView';
import { actions as chatActions } from '../modules/chat';
import { actions as chatsActions } from '../../Chats/modules/chats';
import { actions as userActions } from '../../../reducers/user/local';
import { actions as modalActions } from '../../../reducers/modal';
import styles from '../styles/chat';

const mapStateToProps = ({ chat, user: { local }, loadingBar, cryptos }) => ({
  chat,
  userId: local._id,
  isLoading: !!loadingBar.default,
  currentPrice: cryptos.bitcoin,
});

const mapDispatchToProps = {
  ...chatActions,
  ...chatsActions,
  ...userActions,
  ...modalActions,
};

const StyledChatView = withStyles(styles)(ChatView);

export default connect(mapStateToProps, mapDispatchToProps)(StyledChatView);
