import ChatContainer from './containers/ChatContainer';

export { default as chatReducer } from './modules/chat';

export default ChatContainer;
