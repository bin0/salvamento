import createReducer from '../../../redux/create-reducer';
import {
  GET_USERS_CONVERSATION,
  GET_CHAT_MESSAGES,
  SET_NEW_CHAT_MESSAGE
} from '../../../action-types';
import axios from '../../../config/axios';

export const findChat = remoteUserId => async dispatch => {
  dispatch({ type: GET_USERS_CONVERSATION.REQUEST });

  try {
    const URL = `chat/current/${remoteUserId}`;
    const { data } = await axios(URL);

    dispatch({ type: GET_USERS_CONVERSATION.SET, payload: data });
    dispatch({ type: GET_USERS_CONVERSATION.SUCCESS });
  } catch (error) {
    console.log('error =>', error);
    dispatch({ type: GET_USERS_CONVERSATION.FAILURE });
  }
};

const resetChat = () => dispatch => {
  dispatch({ type: GET_USERS_CONVERSATION.REQUEST });
  dispatch({ type: GET_USERS_CONVERSATION.SUCCESS });
  dispatch({ type: GET_CHAT_MESSAGES.REQUEST });
  dispatch({ type: GET_CHAT_MESSAGES.SUCCESS });
};

export const setNewMessage = message => dispatch => {
  dispatch({ type: SET_NEW_CHAT_MESSAGE.SET, payload: message });
};

export const getChatMessages = chatId => async dispatch => {
  dispatch({ type: GET_CHAT_MESSAGES.REQUEST });

  try {
    const URL = `chat/${chatId}`;
    const { data } = await axios(URL);

    dispatch({ type: GET_USERS_CONVERSATION.SET, payload: data });
    dispatch({ type: GET_CHAT_MESSAGES.SUCCESS });
  } catch (err) {
    console.error(err);
    dispatch({ type: GET_CHAT_MESSAGES.FAILURE });
  }
};

export const actions = {
  findChat,
  resetChat,
  getChatMessages
};

const INITIAL_STATE = {};

const ACTION_HANDLERS = {
  [SET_NEW_CHAT_MESSAGE.SET]: (state, { payload }) => ({
    ...state,
    messages: state.messages.concat(payload)
  }),
  [GET_USERS_CONVERSATION.REQUEST]: () => INITIAL_STATE,
  [GET_USERS_CONVERSATION.SET]: (state, { payload }) => ({
    ...state,
    ...payload
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
