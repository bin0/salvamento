import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getChatMessages } from "../modules/chat";
import socket from "../../../modules/socket";
import { setConnectedUsersCount } from "../../../reducers/users";

export default function useSocket() {
  const email = useSelector(state => state.user.local.email);
  const dispatch = useDispatch();
  //to manage errors
  const [currentId, setCurrentId] = useState(null);
  const [loading, setLoading] = useState(true);
  //to manage async actions

  useEffect(() => {
    setLoading(true);
    if (socket) {
      setLoading(false);
    }
    return () => {
      if (currentId) {
        removeChatListeners();
        socket.emit("leave-room", currentId);
      }
    };
  }, [currentId]);

  const initChat = chatId => {
    if (chatId) {
      removeChatListeners();
      setCurrentId(chatId);
      initChatListeners(chatId);
    }
  };

  const removeChatListeners = () => {
    socket.removeAllListeners();
  };

  const initChatListeners = chatId => {
    socket.emit("user-connected", email);

    socket.on(chatId, () => {
      dispatch(getChatMessages(chatId));
    });

    socket.on("updated-messages", (err, response) => {
      if (err) {
        console.error("Error =>", err);
      }
      if (response) {
        dispatch(getChatMessages(chatId));
      }
    });

    socket.on("connected-users-count", count => {
      dispatch(setConnectedUsersCount(count));
    });
  };

  const sendChatMessage = (message, userId) => {
    if (currentId) {
      socket.emit("new-message", currentId, message, userId);
    }
  };

  const setMessagesRead = messages => {
    const messageIds = messages.map(m => m._id);

    socket.emit("message-read", messageIds);
  };

  return {
    initChat,
    initChatListeners,
    sendChatMessage,
    currentId,
    loading,
    setMessagesRead
  };
}
