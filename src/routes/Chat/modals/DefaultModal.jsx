import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const RecoveryPasswordModal = ({ close, currentPrice, show, sellAmount }) => {
  const handleClose = () => {
    close();
  };

  const currentChange = (sellAmount * currentPrice.EUR.sell).toFixed(2);

  return (
    <Dialog
      open={show}
      aria-labelledby="send-crypto-dialog"
      onClose={handleClose}
    >
      <DialogTitle
        onClose={handleClose}
        aria-labelledby="send-crypto-dialog-title"
      >
        ¿Cuánto quieres enviar?
      </DialogTitle>
      <DialogContent>
        <Typography paragraph>
          Vas a enviar {sellAmount} Bitcoins con valor de {currentChange} Euros
        </Typography>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>cancelar</Button>
        <Button onClock={handleClose}>enviar</Button>
      </DialogActions>
    </Dialog>
  );
};

RecoveryPasswordModal.propTypes = {
  close: PropTypes.func.isRequired,
  currentPrice: {
    EUR: {
      sell: 0
    }
  },
  show: PropTypes.bool.isRequired,
  sellAmount: PropTypes.number.isRequired
};

export default RecoveryPasswordModal;
