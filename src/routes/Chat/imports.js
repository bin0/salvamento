import isEmpty from 'lodash/isEmpty';
import ListItem from '@material-ui/core/ListItem';
import List from '@material-ui/core/List';
import Card from '@material-ui/core/Card';
import { CircularProgress, Typography } from '@material-ui/core';
import InputMessage from './components/InputMessage';
import Messages from './components/Messages';
import ErrorBoundaries from '../../components/ErrorBoundaries';
import { ChatViewContainer, ChatHeader, ChatContent, ChatFooter } from './styles/chat';
import ChatContext from './context';
import useSocket from './hooks/useSocket';

export const imports = {
  InputMessage,
  Messages,
  ChatContent,
  ChatFooter,
  ChatHeader,
  ChatViewContainer,
  ChatContext,
  ErrorBoundaries,
  List,
  ListItem,
  isEmpty,
  Card,
  CircularProgress,
  Typography,
  useSocket,
};
