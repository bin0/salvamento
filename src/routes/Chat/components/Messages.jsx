import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { withStyles } from '@material-ui/core/styles';
import isEmpty from 'lodash/isEmpty';
import { imports as I } from '../imports';
import styles, { MessageInfo } from '../styles/messages';
import Typography from '@material-ui/core/Typography';
import Check from '@material-ui/icons/Check';
import { getChatMessages } from '../modules/chat';
import useSocket from '../hooks/useSocket';

const Messages = ({ classes, messages, chatRef, remoteUser }) => {
  const { setMessagesRead } = useSocket();

  useEffect(() => {
    if (chatRef.current) {
      chatRef.current.scrollTop = chatRef.current.scrollHeight;
    }

    const unReadMessages = messages.filter((m) => m.user === remoteUser && m.read === false);
    if (unReadMessages.length) {
      setMessagesRead(unReadMessages);
    }
  }, [chatRef, messages, remoteUser, setMessagesRead]);

  const renderList = (local) => {
    return (
      <I.List className={classes.list}>
        {messages.map(({ user, _id, text, createdAt, read, sent }) => {
          const createdDate = moment(createdAt);
          let messageData;
          if (createdDate.isSame(new Date(), 'day')) {
            messageData = createdDate.format('HH:mm');
          } else {
            messageData = createdDate.fromNow();
          }
          return (
            <I.ListItem key={_id} className={user === local ? classes.local : classes.remote}>
              <div>
                <div>
                  <Typography variant='body2'>{text}</Typography>
                </div>
                <MessageInfo justify={local === user && 'flex-end'}>
                  <Typography variant='caption'>{messageData}</Typography>

                  {sent && user === local && (
                    <Check
                      className={read ? classes.doubleCheck : classes.check}
                      fontSize='small'
                    />
                  )}
                  {sent && read && user === local && <Check fontSize='small' />}
                </MessageInfo>
              </div>
            </I.ListItem>
          );
        })}
      </I.List>
    );
  };

  return (
    <I.ChatContext.Consumer>
      {({ local }) => !isEmpty(messages) && renderList(local)}
    </I.ChatContext.Consumer>
  );
};

Messages.propTypes = {
  messages: PropTypes.array.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ chat: { messages } }) => ({ messages });

const mapDispatchToProps = { getChatMessages };

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Messages));
