import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { imports as I } from '../imports';
import ChatBox from './ChatBox';
import usePushNotifications from '../../../hooks/usePushNotifications';
import { Button } from '@material-ui/core';

const ChatView = ({
  resetChat,
  location: {
    state: {
      target: { user: remoteUserId },
    },
  },
  chat,
  history,
  findChat,
  ...rest
}) => {
  if (I.isEmpty(chat) && !remoteUserId) {
    history.push('/');
  }

  const { userConsent, onClickAskUserPermission } = usePushNotifications();

  useEffect(() => {
    if (remoteUserId) {
      findChat(remoteUserId);
    }
  }, [findChat, remoteUserId]);

  useEffect(() => {
    return () => {
      resetChat();
    };
  }, [resetChat]);

  if (!userConsent) {
    return (
      <div>
        <p>Aún no has concedido permiso para enviar/recibir notificaciones.</p>
        <Button onClick={onClickAskUserPermission} variant='contained'>
          Permitir notificaciones
        </Button>
      </div>
    );
  }
  return !I.isEmpty(chat) && <ChatBox {...rest} chat={chat} history={history} />;
};

ChatView.propTypes = {
  resetChat: PropTypes.func.isRequired,
  location: PropTypes.shape({
    state: PropTypes.shape({
      target: PropTypes.shape({
        user: PropTypes.string,
      }),
    }),
  }),
  userId: PropTypes.string.isRequired,
  chat: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  findChat: PropTypes.func.isRequired,
};

export default ChatView;
