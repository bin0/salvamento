import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import CardContent from '@material-ui/core/CardContent';
import InputAdornment from '@material-ui/core/InputAdornment';
import SendIcon from '@material-ui/icons/Send';
import { CircularProgress } from '@material-ui/core';
import styles, { StyledInputMessage } from '../styles/input';
import usePushNotifications from '../../../hooks/usePushNotifications';
import useSocket from '../hooks/useSocket';

const InputMessage = ({ classes, chatRef, chat, userId }) => {
  const {
    userConsent,
    onClickAskUserPermission,
    onClickSendSubscriptionToPushServer,
    userSubscription,
    loading: notificationsLoading
  } = usePushNotifications();
  const { sendChatMessage, currentId, initChat } = useSocket();
  const [message, setMessage] = useState('');

  useEffect(() => {
    if (!currentId) {
      initChat(chat._id);
    }
  }, [chat._id, currentId, initChat]);

  const handleChange = e => {
    if (!message && (!e.target.value || e.target.value === ' ')) {
      return;
    }
    setMessage(e.target.value);
  };

  const handleSubmit = () => {
    if (!chat || !message || !userId) {
      console.error('No se puede enviar el mensaje porque faltan parametros');
    } else {
      sendMessage();
    }
  };

  const sendMessage = () => {
    sendChatMessage(message, userId);
    if (userConsent === 'default') {
      onClickAskUserPermission();
    } else {
      if (!userSubscription) {
        console.log('OJO! Falta la subscripcion de notificaciones de usuario');
      } else {
        console.log('Enviando subscripción de notificaciones de usuario');
        onClickSendSubscriptionToPushServer();
      }
    }
    setMessage('');
    chatRef.current.scrollTop = chatRef.current.scrollHeight;
  };

  const handleKeyDown = e => {
    if (!message && (e.key === 'Space' || e.keyCode === 32)) {
      return;
    }
    if (e.key === 'Enter' || e.keyCode === 13) {
      if (!message) {
        return;
      }
      sendMessage();
    }
  };

  if (notificationsLoading) {
    return <CircularProgress size={60} color="secondary" thickness={1.6} />;
  }

  return (
    <StyledInputMessage>
      <CardContent className={classes.cardContent}>
        <TextField
          className={classes.textField}
          color="primary"
          autoFocus
          fullWidth
          margin="none"
          onChange={handleChange}
          placeholder="Escribe un mensaje..."
          value={message}
          variant="standard"
          onKeyDown={handleKeyDown}
          InputProps={{
            endAdornment: (
              <InputAdornment className={classes.adornment} position="end" onClick={handleSubmit}>
                <SendIcon />
              </InputAdornment>
            )
          }}
          inputProps={{
            maxLength: 255
          }}
        />
      </CardContent>
    </StyledInputMessage>
  );
};

InputMessage.propTypes = {
  classes: PropTypes.object.isRequired,
  chatRef: PropTypes.object.isRequired
};

export default withStyles(styles)(InputMessage);
