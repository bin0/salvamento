import React, { createRef } from "react";
import PropTypes from "prop-types";
import { imports as I } from "../imports";
import { CardHeader } from "@material-ui/core";

const ChatBox = ({ classes, chat, userId }) => {
  const chatRef = createRef();
  const remoteUser = chat.owner._id === userId ? chat.receiver : chat.owner;

  return (
    <I.Card className={classes.card}>
      <I.ErrorBoundaries>
        <I.ChatContext.Provider value={{ local: userId }}>
          <I.ChatViewContainer>
            {remoteUser && (
              <I.ChatHeader>
                <CardHeader
                  subheader={remoteUser.isOnline ? "Conectado" : "Desconectado"}
                  title={
                    remoteUser.displayName || remoteUser.email || "Desconocido"
                  }
                />
              </I.ChatHeader>
            )}
            <I.ChatContent ref={chatRef}>
              <I.Messages chatRef={chatRef} remoteUser={remoteUser._id} />
            </I.ChatContent>
            <I.ChatFooter>
              <I.InputMessage
                chat={chat}
                chatRef={chatRef}
                userId={userId}
                remoteUser={remoteUser}
              />
            </I.ChatFooter>
          </I.ChatViewContainer>
        </I.ChatContext.Provider>
      </I.ErrorBoundaries>
    </I.Card>
  );
};

ChatBox.propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
  chat: PropTypes.shape({
    _id: PropTypes.string.isRequired
  }).isRequired
};

export default ChatBox;
