import styled from 'styled-components/macro';

export const ChatViewContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ChatHeader = styled.div`
  /* border-bottom: 1px solid grey;
  display: flex;
  flex-direction: column;
  padding-bottom: 24px;
  font-style: italic; */
`;

export const ChatContent = styled.div`
  display: flex;
  overflow: auto;
  max-height: 10vh;
  max-height: 60vh;
  border-bottom: 1px solid grey;
  scroll-behavior: smooth;
`;

export const ChatFooter = styled.div`
  display: flex;
`;

export default (theme) => ({
  root: {
    padding: 0,
  },
  adornment: {
    cursor: 'pointer',
  },
  card: {
    background: theme.palette.secondary.main,
    padding: theme.spacing(3),
    width: 640,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
});
