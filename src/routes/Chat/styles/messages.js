import styled from 'styled-components/macro';

export const MessageInfo = styled.div`
  display: flex;
  justify-content: ${({ justify }) => justify && justify};
  align-items: flex-end;
`;

export default theme => ({
  list: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    paddingLeft: '.1rem'
  },
  local: {
    alignSelf: 'flex-end',
    background: theme.palette.primary.main,
    borderRadius: theme.spacing(1),
    color: theme.palette.primary.contrastText,
    display: 'flex',
    justifyContent: 'flex-end',
    marginBottom: '.5rem',
    width: '80%'
  },
  remote: {
    alignSelf: 'flex-start',
    background: theme.palette.secondary.main,
    borderRadius: theme.spacing(1),
    boxShadow: '0 0 4px 0px #282c33',
    color: theme.palette.secondary.contrastText,
    display: 'flex',
    justifyContent: 'flex-start',
    marginBottom: '.5rem',
    width: '80%'
  },
  check: {
    color: 'red'
  },
  doubleCheck: {
    marginRight: '-0.84rem'
  }
});
