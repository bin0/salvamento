import styled from 'styled-components/macro';

export const StyledInputMessage = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export default () => ({
  textField: {},
  cardContent: {
    width: '100%',
    margin: '1rem 0',
    padding: 0
  },
  cardActions: {
    alignSelf: 'flex-end',
    margin: 0,
    padding: 0
  }
});
