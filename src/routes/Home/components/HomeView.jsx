import React, { useCallback } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Typography } from '@material-ui/core';
import PrimaryButton from '../../../components/PrimaryButton';

const HomeView = ({ history, authenticated, i18n }) => {
  const handleClick = useCallback(
    route => () => {
      history.push(route);
    },
    [history]
  );

  return (
    <>
      <Typography color="primary" variant="h1">
        {i18n.get('app.title')}
      </Typography>
      <Typography color="primary" variant="h2">
        {i18n.get('app.subtitle')}
      </Typography>
      <div style={{ marginTop: '1rem' }}>
        {!authenticated ? (
          <>
            <PrimaryButton style={{ marginRight: '1rem' }} onClick={handleClick('/login/register')}>
              {i18n.get('register')}
            </PrimaryButton>
            <PrimaryButton onClick={handleClick('/login')}>{i18n.get('instant.access')}</PrimaryButton>
          </>
        ) : (
          <>
            <PrimaryButton style={{ marginRight: '1rem' }} onClick={handleClick('/teams')}>
              {i18n.get('view.teams')}
            </PrimaryButton>
            <PrimaryButton onClick={handleClick('/alerts')}>{i18n.get('view.alerts')}</PrimaryButton>
          </>
        )}
      </div>
    </>
  );
};

HomeView.propTypes = {
  history: PropTypes.object.isRequired
};

const mapStateToProps = ({ i18n, auth }) => ({
  i18n,
  authenticated: auth.authenticated
});

export default connect(mapStateToProps)(HomeView);
