import { connect } from "react-redux";
import HomeView from "../components/HomeView";

const mapStateToProps = ({ i18n, auth }) => ({
  i18n,
  authenticated: auth.authenticated
});

export default connect(mapStateToProps)(HomeView);
