import styled from 'styled-components';

export const AlertsContainerStyled = styled.div`
  div {
    margin-top: 2rem;
  }
`;
