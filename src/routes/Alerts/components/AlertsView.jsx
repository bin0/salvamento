import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { CircularProgress, Button } from '@material-ui/core';
import { AlertsContainerStyled } from '../styles/alerts';

const Error = props => <div>{props.children}</div>;
const AlertsView = ({ getAlerts, isLoading, alerts: { error }, i18n }) => {
  useEffect(() => {
    getAlerts();
  }, [getAlerts]);

  if (isLoading) {
    return <CircularProgress thickness={0.8} size={60} color="secondary" />;
  }
  if (error) {
    return <Error>{error}</Error>;
  }
  return (
    <AlertsContainerStyled>
      <p>{i18n.get('app.noActiveAlerts')}</p>
      <div>
        <Button variant="contained" color="primary">
          {i18n.get('app.activateAlert')}
        </Button>
      </div>
    </AlertsContainerStyled>
  );
};

AlertsView.propTypes = {
  alerts: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  })
};

export default AlertsView;
