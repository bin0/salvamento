import { connect } from 'react-redux';
import AlertsView from '../components/AlertsView';
import { actions as alertsActions } from '../modules/alerts';

const mapStateToProps = ({ alerts, loadingBar, i18n }) => ({
  alerts,
  isLoading: !!loadingBar.default,
  i18n
});

const mapDispatchToProps = { ...alertsActions };

export default connect(mapStateToProps, mapDispatchToProps)(AlertsView);
