import createReducer from '../../../redux/create-reducer';
import { SET_ALERTS } from '../../../action-types';
// import { openSnack } from '../../../reducers/snack';
// import axios from '../../../config/axios';

const setAlerts = (type, payload) => dispatch => dispatch({ type, payload });

const getAlerts = () => dispatch => {
  dispatch(setAlerts(SET_ALERTS.REQUEST));

  setTimeout(() => {
    // dispatch(openSnack('Data Ready'));
    dispatch(setAlerts(SET_ALERTS.SUCCESS));
  }, 3000);
  // setTimeout(() => {
  //   dispatch(openSnack('Custom Error'));
  //   dispatch(setAlerts(SET_ALERTS.FAILURE, 'Custom Error'));
  // }, 3000);
};

export const actions = {
  getAlerts
};

export const INITIAL_STATE = {
  data: [],
  error: ''
};

export const ACTION_HANDLERS = {
  [SET_ALERTS.FAILURE]: (state, { payload }) => ({ ...state, error: payload })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
