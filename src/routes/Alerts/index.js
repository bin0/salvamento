import React from 'react';
import { Route } from 'react-router-dom';
import AlertContainer from './container/AlertsContainer';

const ROUTE_PREFIX = '/alerts';

export default ({ location }) => {
  switch (location.pathname) {
    case ROUTE_PREFIX:
      return <Route exact component={AlertContainer} />;
    case `${ROUTE_PREFIX}/new`:
      return <Route component={AlertContainer} />;
    default:
      return <Route component={AlertContainer} />;
  }
};
