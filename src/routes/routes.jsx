import React from 'react';

// import ChatLayout from '../layouts/ChatLayout';
import MainLayout from '../layouts/MainLayout';
import PublicLayout from '../layouts/PublicLayout';
import PushNotifications from './PushNotifications';
import RouteWithLayout from './RouteWithLayout';
import NotFoundPage from '../components/404';

const Alerts = React.lazy(() => import('./Alerts'));
// const Chat = React.lazy(() => import('./Chat'));
// const Chats = React.lazy(() => import('./Chats'));
const Dashboard = React.lazy(() => import('./Dashboard'));
const Help = React.lazy(() => import('./Help'));
const Home = React.lazy(() => import('./Home'));
const Login = React.lazy(() => import('./Login'));
const Options = React.lazy(() => import('./Options'));
const Teams = React.lazy(() => import('./Teams'));
const User = React.lazy(() => import('./User'));

export default () => [
  <RouteWithLayout key="/" path="/" exact component={Home} layout={MainLayout} />,
  <RouteWithLayout key="alerts" path="/alerts" component={Alerts} layout={MainLayout} />,
  // <RouteWithLayout key="chat" path="/chat" exact component={Chat} layout={MainLayout} />,
  // <RouteWithLayout key="chats" path="/chats" exact component={Chats} layout={ChatLayout} />,
  <RouteWithLayout key="dashboard" path="/dashboard" exact component={Dashboard} layout={MainLayout} />,
  <RouteWithLayout key="login" path="/login" component={Login} layout={PublicLayout} />,
  <RouteWithLayout key="help" path="/help" exact component={Help} layout={PublicLayout} />,
  <RouteWithLayout key="options" path="/options" exact component={Options} layout={MainLayout} />,
  <RouteWithLayout key="push" path="/push" exact component={PushNotifications} layout={MainLayout} />,
  <RouteWithLayout key="user" path="/user" component={User} layout={MainLayout} />,
  <RouteWithLayout key="teams" path="/teams" component={Teams} layout={MainLayout} />,
  <RouteWithLayout key="*" path="/*" component={NotFoundPage} layout={PublicLayout} />
];
