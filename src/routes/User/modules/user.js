import createReducer from '../../../redux/create-reducer';
import axios from '../../../config/axios';
import { GET_TEAM_USER } from '../../../action-types';

const getTeamUser = userId => async dispatch => {
  dispatch({ type: GET_TEAM_USER.REQUEST });

  try {
    const URL = `/user/${userId}`;
    const response = await axios(URL);
    dispatch({ type: GET_TEAM_USER.SET, payload: response.data });
    dispatch({ type: GET_TEAM_USER.SUCCESS });
  } catch (err) {
    dispatch({ type: GET_TEAM_USER.FAILURE, payload: err.response.data.message });
  }
};

export const actions = {
  getTeamUser
};

export const INITIAL_STATE = {
  data: {},
  error: ''
};

export const ACTION_HANDLERS = {
  [GET_TEAM_USER.REQUEST]: () => INITIAL_STATE,
  [GET_TEAM_USER.SET]: (state, { payload }) => ({ ...state, data: payload }),
  [GET_TEAM_USER.FAILURE]: (state, { payload }) => ({ ...state, error: payload })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
