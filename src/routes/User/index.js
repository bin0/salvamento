import React from 'react';
import UserContainer from './container/UserContainer';
import { Switch, Route } from 'react-router-dom';

export { default as reducer } from './modules/user';

export default () => (
  <Switch>
    <Route path="/user" exact component={UserContainer} />
    <Route path="/user/:id" exact component={UserContainer} />
  </Switch>
);
