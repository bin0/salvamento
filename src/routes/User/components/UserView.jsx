import React from 'react';
import PropTypes from 'prop-types';
import { useEffect } from 'react';

const UserView = ({
  teamUser,
  match: {
    params: { id }
  },
  getTeamUser
}) => {
  useEffect(() => {
    getTeamUser(id);
  }, [getTeamUser, id]);

  if (teamUser.error) {
    return <div>{teamUser.error}</div>;
  }

  return <div>{teamUser.displayName}</div>;
};

UserView.propTypes = {
  teamUser: PropTypes.object.isRequired,
  getTeamUser: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }).isRequired
};

export default UserView;
