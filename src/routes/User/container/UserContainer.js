import { connect } from 'react-redux';
import UserView from '../components/UserView';
import { actions as userActions } from '../modules/user';

const mapStateToProps = ({ teamUser }) => ({ teamUser: teamUser.data });

const mapDispatchToProps = { ...userActions };

export default connect(mapStateToProps, mapDispatchToProps)(UserView);
