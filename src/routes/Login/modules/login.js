import createReducer from '../../../redux/create-reducer';
import axios from '../../../config/axios';
import history from '../../../redux/history';
import { SET_UNLOCK_CODE } from '../../../action-types';

export const getUnlockCode = (email, unlockCode) => async dispatch => {
  dispatch({ type: SET_UNLOCK_CODE.REQUEST });
  try {
    const URL = '/login/unlock-code';
    const { data: token } = await axios.post(URL, { email, unlockCode });
    console.log('token', token);
    axios.defaults.headers.Authorization = `Bearer ${token}`;
    localStorage.setItem('wc-token', token);

    dispatch({ type: SET_UNLOCK_CODE.SUCCESS });
    history.push('/dashboard');
  } catch (err) {
    console.log('err', err);
    dispatch({ type: SET_UNLOCK_CODE.FAILURE });
  }
};

export const actions = {
  getUnlockCode
};

export const INITIAL_STATE = {
  isLoginIn: false,
  unlockCode: null
};

export const ACTION_HANDLERS = {};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
