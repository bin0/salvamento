import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { withStyles, FormGroup } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch } from 'react-redux';
import { getUnlockCode } from '../modules/login';
import { createRef } from 'react';

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  }
});

const modalStyles = theme => ({
  numberField: {
    fontSize: 60
  },
  numberContainer: {
    width: '150px'
  }
});

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const RecoveryPasswordModal = ({ classes, close, email, show }) => {
  const sendButtonRef = createRef();
  const [code, setCode] = useState('');
  const [disabled, setDisabled] = useState(true);
  const dispatch = useDispatch();

  const handleClose = () => {
    close();
  };

  const handleSendCode = () => {
    dispatch(getUnlockCode(email, code));
  };

  const handleChange = event => {
    const { value } = event.target;
    if (Number(value) || value === '') setCode(value);
    if (value.length === 4) {
      setDisabled(false);
    } else setDisabled(true);
  };

  const handleKeyPress = event => {
    if (event.key === 'Enter' && code.length === 4) {
      handleClose();
    }
  };

  return (
    <Dialog open={show} aria-labelledby="send-crypto-dialog" onClose={handleClose}>
      <DialogTitle onClose={handleClose} aria-labelledby="send-crypto-dialog-title">
        ¡Email enviado!
      </DialogTitle>
      <DialogContent>
        Introduce el código de 4 dígitos que te hemos enviado
        <FormGroup>
          <TextField
            autoFocus
            className={classes.numberContainer}
            inputProps={{
              inputmode: 'numeric',
              pattern: '[0-9]*',
              maxlength: '4'
            }}
            InputProps={{
              className: classes.numberField
            }}
            onChange={handleChange}
            onKeyPress={handleKeyPress}
            size="medium"
            type="text"
            value={code}
          />
        </FormGroup>
      </DialogContent>
      <DialogActions>
        <Button ref={sendButtonRef} color="primary" variant="contained" disabled={disabled} onClick={handleSendCode}>
          enviar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const RecoveryPasswordModalStyled = withStyles(modalStyles)(RecoveryPasswordModal);

RecoveryPasswordModal.propTypes = {
  close: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  email: PropTypes.string.isRequired
};

export default RecoveryPasswordModalStyled;
