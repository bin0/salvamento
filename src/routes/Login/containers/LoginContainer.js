import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import LogInView from '../components/LoginView';
import { actions as authActions } from '../../../reducers/auth';
import { actions as snackActions } from '../../../reducers/snack';
import styles from '../styles/login';

const mapStateToProps = ({ auth, i18n }) => ({
  isLoading: auth.isAuthenticating,
  i18n
});

const mapDispatchToProps = {
  ...authActions,
  ...snackActions
};

const StyledLogIn = withStyles(styles)(LogInView);

export default connect(mapStateToProps, mapDispatchToProps)(StyledLogIn);
