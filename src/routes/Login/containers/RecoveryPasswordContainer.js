import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import RecoveryPassword from '../components/RecoveryPassword';
import { sendPasswordResetEmail } from '../../../reducers/auth';
import { actions as modalActions } from '../../../reducers/modal';
import { openSnack } from '../../../reducers/snack';
import styles from '../styles/recovery';

const mapStateToProps = ({ loadingBar, login }) => ({
  isLoading: !!loadingBar.default,
  unlockCode: login.unlockCode
});

const mapDispatchToProps = {
  sendPasswordResetEmail,
  openSnack,
  ...modalActions
};

const StyledLogIn = withStyles(styles)(RecoveryPassword);

export default connect(mapStateToProps, mapDispatchToProps)(StyledLogIn);
