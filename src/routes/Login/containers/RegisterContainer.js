import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';

import RegisterView from '../components/RegisterView';
import { createNewUser } from '../../../reducers/user/local';
import styles from '../styles/register';

const mapStateToProps = ({ auth, i18n }) => ({
  authenticated: auth.authenticated,
  isAuthenticating: auth.isAuthenticating,
  i18n
});

const mapDispatchToProps = { createNewUser };

const StyledLogIn = withStyles(styles)(RegisterView);

export default connect(mapStateToProps, mapDispatchToProps)(StyledLogIn);
