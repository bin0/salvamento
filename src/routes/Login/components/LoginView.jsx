import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import AddIcon from '@material-ui/icons/Add';
import CircularProgress from '@material-ui/core/CircularProgress';
import CardHeader from '@material-ui/core/CardHeader';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import SignInWithEmail from './SignInWithEmail';

const LogInView = ({ isLoading, classes, history, ...rest }) => {
  const handleRegisterClick = useCallback(() => {
    history.push('/login/register');
  }, [history]);

  if (isLoading) {
    return <CircularProgress size={60} color="secondary" thickness={1.6} />;
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        className={classes.header}
        title="Accede Ahora"
        subheader="sino eres todavía usuario, ¡conéctate!"
        action={
          <Fab onClick={handleRegisterClick} color="secondary">
            <Tooltip title="Alta de nuevos usuarios" placement="top">
              <AddIcon />
            </Tooltip>
          </Fab>
        }
      />
      <SignInWithEmail classes={classes} {...rest} history={history} />
    </Card>
  );
};

LogInView.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }),
  isLoading: PropTypes.bool.isRequired
};

export default LogInView;
