import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';

import EmailPassword from '../../../components/Forms/EmailPassword';

function Register({ authenticated, history, ...rest }) {
  if (authenticated) {
    history.push('/');
  }
  return (
    <Card>
      <CardHeader
        title="Crea una cuenta para acceder"
        subheader="Si te parece bien... claro está."
      />
      <EmailPassword {...rest} />
    </Card>
  );
}

Register.propTypes = {
  classes: PropTypes.object.isRequired,
  createNewUser: PropTypes.func.isRequired,
  isAuthenticating: PropTypes.bool.isRequired,
  authenticated: PropTypes.bool.isRequired
};

export default Register;
