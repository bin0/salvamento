// import React from 'react';
// import PropTypes from 'prop-types';
// import { CardHeader } from '@material-ui/core';
// import Button from '@material-ui/core/Button';
// import Card from '@material-ui/core/Card';
// import CardActions from '@material-ui/core/CardActions';
// import Register from './Register';

// const NoUser = ({
//   isRegistering,
//   isSigninIn,
//   setRegistering,
//   setSigninIn,
//   classes,
// }) => {
//   return (
//     <Card className={classes.card}>
//       {!isRegistering && !isSigninIn && (
//         <CardHeader title="¡Bienvenido!" subheader="Elige la forma de acceso" />
//       )}
//       {!isRegistering && !isSigninIn && (
//         <CardActions>
//           <Button
//             variant="contained"
//             color="primary"
//             className={classes.button}
//             onClick={setRegistering}
//           >
//             crear usuario
//           </Button>
//           <Button
//             variant="contained"
//             color="primary"
//             className={classes.button}
//             onClick={setSigninIn}
//           >
//             acceder
//           </Button>
//         </CardActions>
//       )}
//       {isRegistering && <Register />}
//       {isSigninIn && <SignIn />}
//     </Card>
//   );
// };

// NoUser.propTypes = {
//   classes: PropTypes.shape({
//     button: PropTypes.string.isRequired,
//     card: PropTypes.string.isRequired,
//   }).isRequired,
//   isRegistering: PropTypes.bool.isRequired,
//   isSigninIn: PropTypes.bool.isRequired,
//   setRegistering: PropTypes.func.isRequired,
//   setSigninIn: PropTypes.func.isRequired,
// };

// export default NoUser;
