import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import { validateInput } from '../../../utils/validations';

const RecoveryPasswordView = ({ history, classes, sendPasswordResetEmail, openSnack, openModal }) => {
  const [email, setEmail] = useState('');
  const [disabled, setDisabled] = React.useState(true);
  const [error, setError] = useState(false);

  const handleRecoverPassword = () => {
    if (!email) {
      setError(true);
    }
    sendPasswordResetEmail(email);
    openSnack(
      `Revisa el buzón de ${email}, te hemos enviado un correo con las instrucciones para restablecer tu contraseña.`
    );
    setEmail('');
    openModal({ modalType: 'RECOVERY_PASSWORD', modalProps: { email } });
    history.push('/login');
  };

  const handleKeyDown = event => {
    if (event.key === 'Enter' && validateInput('email', email)) {
      handleRecoverPassword();
    }
  };

  const checkEmail = e => {
    setError(!validateInput('email', e.target.value));
    setDisabled(!validateInput('email', e.target.value));
  };

  return (
    <Card>
      <CardHeader title="Recupera tu acceso" subheader="Introduce tu email para recuperar el acceso." />
      <CardContent>
        <TextField
          autoComplete=""
          autoFocus
          error={error}
          fullWidth
          helperText={disabled && 'Introduce un email válido'}
          id="outlined-email"
          label="Correo Electrónico"
          margin="normal"
          onBlur={checkEmail}
          onChange={e => setEmail(e.target.value)}
          onKeyDown={handleKeyDown}
          onPaste={checkEmail}
          type="email"
          value={email}
          variant="standard"
        />
      </CardContent>
      <CardActions>
        <Button
          color="secondary"
          disabled={disabled}
          id="recover-password"
          onClick={handleRecoverPassword}
          variant="contained"
        >
          recuperar contraseña
        </Button>
      </CardActions>
    </Card>
  );
};

RecoveryPasswordView.propTypes = {
  classes: PropTypes.object.isRequired,
  sendPasswordResetEmail: PropTypes.func.isRequired,
  openSnack: PropTypes.func.isRequired,
  openModal: PropTypes.func.isRequired
};

export default RecoveryPasswordView;
