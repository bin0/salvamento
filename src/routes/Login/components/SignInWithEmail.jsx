import React, { useEffect } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import { validateInput } from '../../../utils/validations';
import { CardActions } from '@material-ui/core';

//! FIXME ADOLFO i18n
const SignInWithEmail = ({ classes, logIn: handleSubmit, history, i18n }) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [disabled, setDisabled] = React.useState(true);
  const [fullfilled, setFullfiled] = React.useState(false);

  useEffect(() => {
    const validEmail = validateInput('email', email);
    const validPassword = password.length > 5;

    setDisabled(!validEmail || !validPassword);
    setFullfiled(validEmail && validPassword);
  }, [email, password]);

  const onSignInSubmit = () => {
    setDisabled(true);
    handleSubmit(email, password);
    setEmail('');
    setPassword('');
  };

  const handleKeyDown = event => {
    if (event.key === 'Enter' && fullfilled) {
      onSignInSubmit();
    }
  };

  return (
    <>
      <CardContent>
        <TextField
          autoFocus
          className={classes.textField}
          error={!validateInput('email', email)}
          helperText={!validateInput('email', email) && 'Introduce un email válido'}
          fullWidth
          id="standard-email"
          label="Correo Electrónico"
          margin="normal"
          onChange={e => setEmail(e.target.value)}
          onKeyDown={handleKeyDown}
          type="email"
          value={email}
          variant="standard"
        />
        <TextField
          className={classes.textField}
          error={password.length < 6}
          fullWidth
          id="standard-password"
          label="Contraseña"
          margin="normal"
          onChange={e => setPassword(e.target.value)}
          onKeyDown={handleKeyDown}
          type="password"
          value={password}
          variant="standard"
        />
      </CardContent>
      <CardActions>
        <Button color="secondary" disabled={disabled} id="sign-in-button" onClick={onSignInSubmit} variant="contained">
          {i18n.get('instant.access')}
        </Button>
        <Button
          color="primary"
          id="recover-password"
          onClick={() => history.push('login/reset-password')}
          variant="contained"
        >
          {i18n.get('recoverPassword')}
        </Button>
      </CardActions>
    </>
  );
};

export default SignInWithEmail;
