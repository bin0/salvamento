export default theme => ({
  textField: {
    marginBottom: theme.spacing(2)
  },
  textPassword: {
    '&:first-child': {
      marginRight: theme.spacing(1)
    }
  }
});
