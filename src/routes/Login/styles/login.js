export default theme => ({
  grow: {
    flexGrow: 1
  },
  list: {
    width: 250
  },
  fullList: {
    width: 'auto'
  },
  footerItem: {
    // width: '33%',
    padding: '1rem',
    border: '1px solid red'
  },
  h5: {},
  menuButton: {
    padding: 0
  },
  input: {
    display: 'none'
  },
  textField: {
    // color: theme.palette.primary.main
    margin: 0
  },
  captcha: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  header: {
    textAlign: 'left'
  }
});
