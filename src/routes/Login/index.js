import React from 'react';
import { Route, Switch } from 'react-router-dom';

import LoginContainer from './containers/LoginContainer';
import RegisterContainer from './containers/RegisterContainer';
import RecoveryPassword from './containers/RecoveryPasswordContainer';

export { default as reducer } from './modules/login';

const ROUTE_PREFIX = '/login';

export default () => (
  <Switch>
    <Route path={ROUTE_PREFIX} exact component={LoginContainer} />
    <Route path={`${ROUTE_PREFIX}/register`} exact component={RegisterContainer} />
    <Route path={`${ROUTE_PREFIX}/reset-password`} exact component={RecoveryPassword} />
  </Switch>
);

export { default as loginReducer } from './modules/login';
