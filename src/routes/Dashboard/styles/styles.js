import styled from 'styled-components/macro';

export const DashboardContainer = styled.section`
  display: flex;
  flex-direction: column;
  padding: 1rem;

  h2 {
    align-self: center;
  }
`;
