import { connect } from 'react-redux';
import DashboardView from '../components/DashboardView';
import { actions as locationActions } from '../../../reducers/user/location';
import { actions as chatActions } from '../../../routes/Chats/modules/chats';
import { actions as snackActions } from '../../../reducers/snack';

const mapStateToProps = ({ i18n }) => ({ i18n });

const mapDispatchToProps = {
  ...locationActions,
  ...chatActions,
  ...snackActions
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardView);
