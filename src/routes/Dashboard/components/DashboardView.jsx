import React, { useEffect, useCallback } from 'react';
import MapComponent from '../../../components/MapComponent';
import useLocation from '../../../hooks/useLocation';
import Button from '@material-ui/core/Button';

const DashboarView = ({ i18n, openSnack }) => {
  useEffect(() => {
    if (navigator.getBattery) {
      navigator.getBattery().then(result => {
        if (!result.charging && result.level <= 0.2) {
          console.log('No está en carga y le queda poca batería');
        } else {
          console.log('Está en carga o tiene batería suficiente');
        }
      });
    }
  }, []);
  const { watchPosition, clearWatching, location, loading, watching } = useLocation();
  const handleWatchPosition = useCallback(() => {
    openSnack(i18n.get('app.map.activatingLocation'));
    watchPosition();
  }, [i18n, openSnack, watchPosition]);

  return (
    <div className="App">
      {loading && i18n.get('home.map.loadingMap')}
      {!loading && location.lat && (
        <MapComponent
          isMarkerShown
          location={location}
          clearWatching={clearWatching}
          watchPosition={handleWatchPosition}
          watching={watching}
          i18n={i18n}
        />
      )}
      {!watching && !location.lat && (
        <Button variant="contained" color="primary" onClick={watchPosition}>
          {i18n.get('app.map.startSearch')}
        </Button>
      )}
    </div>
  );
};

export default DashboarView;
