import createReducer from '../../../redux/create-reducer';
import { GET_USER_CHATS } from '../../../action-types';
import axios from '../../../config/axios';
import { getUnreadMessages } from '../redux/selectors';

const getUserChats = () => async (dispatch, getState) => {
  dispatch({ type: GET_USER_CHATS.REQUEST });

  try {
    const URL = 'chats';
    const { data } = await axios(URL);

    const userId = getState().user.local._id;

    dispatch({ type: GET_USER_CHATS.SUCCESS });
    dispatch({ type: GET_USER_CHATS.SET, payload: { data, userId } });
  } catch (error) {
    console.error(error);
    dispatch({ type: GET_USER_CHATS.FAILURE });
  }
};

export const actions = {
  getUserChats
};

export const INITIAL_STATE = {
  list: [],
  unreadMessagesCount: 0
};

export const ACTION_HANDLERS = {
  [GET_USER_CHATS.SET]: (state, { payload: { data, userId } }) => ({
    ...state,
    list: data,
    unreadMessagesCount: getUnreadMessages(data, userId)
  })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
