import isEmpty from 'lodash/isEmpty';

const itsMine = ({ user }, id) => user === id;
const isUnRead = id => message => !message.read && !itsMine(message, id);

export const getNonEmptyChats = chats => chats.filter(c => !isEmpty(c.messages));
export const getUnreadMessages = (chats, id) => {
  const unreadMessages = chats.reduce((acc, item) => {
    if (item.messages.some(isUnRead(id))) {
      return acc + 1;
    }
    return acc;
  }, 0);

  return unreadMessages;
};
