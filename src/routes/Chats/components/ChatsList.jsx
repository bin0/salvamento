import React, { Fragment, useCallback } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import { getFirstLetter } from '../../../utils/strings';

const ChatsList = ({ history, user, classes, chats }) => {
  const getListItemTextPrimary = ({ receiver, owner }) =>
    user._id === owner._id ? receiver.displayName : owner.displayName;

  const handleClick = (receiver, owner) => {
    history.push('/chat', { target: { user: user._id === owner ? receiver : owner } });
  };

  const messageFilter = useCallback((message) => !message.read && message.user !== user._id, [
    user._id,
  ]);

  return (
    <List className={classes.root}>
      {!isEmpty(chats) ? (
        chats.map(({ _id, receiver, messages, owner }) => {
          const unreadCount = messages.filter(messageFilter);
          return (
            <Fragment key={_id}>
              <ListItem
                className={classes.listItem}
                alignItems='flex-start'
                onClick={() => handleClick(receiver._id, owner._id)}
              >
                <ListItemAvatar>
                  <Badge color='primary' badgeContent={unreadCount.length}>
                    <Avatar className={classes.avatar}>
                      {getFirstLetter(receiver && (receiver.displayName || receiver.email))}
                    </Avatar>
                  </Badge>
                </ListItemAvatar>
                <ListItemText
                  primary={getListItemTextPrimary({ receiver, owner })}
                  secondary={
                    <Typography
                      component='span'
                      variant='body2'
                      className={classes.inline}
                      color='textPrimary'
                    >
                      {!isEmpty(messages) && messages[messages.length - 1].text}
                    </Typography>
                  }
                />
              </ListItem>
              <Divider variant='inset' component='li' />
            </Fragment>
          );
        })
      ) : (
        <Button variant='contained' onClick={() => history.push('/buy')}>
          No tienes conversaciones abiertas
        </Button>
      )}
    </List>
  );
};

ChatsList.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  chats: PropTypes.array.isRequired,
};

export default ChatsList;
