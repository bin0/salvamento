import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ChatList from './ChatsList';
import { ChatViewDesktop, ChatViewMobile, LeftSidebar, ChatViewContent } from '../styles';
import { withWidth } from '@material-ui/core';

const ChatsView = ({ getUserChats, width, ...rest }) => {
  useEffect(() => {
    getUserChats();
  }, [getUserChats]);

  return width !== 'xs' ? (
    <ChatViewDesktop>
      <LeftSidebar>
        <ChatList {...rest} />
      </LeftSidebar>
      <ChatViewContent>
        Haz clic sobre una conversación para enviar un mensaje a otro usuario.
      </ChatViewContent>
    </ChatViewDesktop>
  ) : (
    <ChatViewMobile>
      <ChatList {...rest} />
    </ChatViewMobile>
  );
};

ChatsView.propTypes = {
  getUserChats: PropTypes.func.isRequired,
};

export default withWidth()(ChatsView);
