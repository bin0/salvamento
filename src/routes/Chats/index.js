import Chats from './containers/ChatsContainer';

export { default as reducer } from './modules/chats';

export default Chats;
