import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import ChatsView from '../components/ChatsView';
import { actions } from '../modules/chats';
import styles from '../styles';

const mapStateToProps = ({ chats, user: { local } }) => ({
  chats: chats.list,
  user: local
});

const mapDispatchToProps = { ...actions };

const StyledChats = withStyles(styles)(ChatsView);

const withRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledChats);

export default withRedux;
