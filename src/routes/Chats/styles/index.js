import styled from 'styled-components/macro';

export const StyledComponent = styled.div;

export default (theme) => ({
  root: {
    padding: '1rem 0',
  },
  avatar: {
    backgroundColor: 'orange',
  },
  inline: {
    display: 'flex',
    fontSize: '.725rem',
    lineHeight: '0.93rem',
    overflow: 'hidden',
  },
  listItem: {
    cursor: 'pointer',
    background: '#6c7077',
    maxHeight: '67px',

    '&:hover': {
      backgroundColor: 'orange',
    },
  },
});

export const ChatViewDesktop = styled.div`
  align-self: flex-start;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ChatViewMobile = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LeftSidebar = styled.div`
  width: 300px;
  height: calc(100vh - 90px);
  border-right: 1px solid rgba(0, 0, 0, 0.2);
`;

export const ChatViewContent = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  width: 300px;
  padding: 1rem;
`;
