import { createContext } from 'react';

const initialValue = {};

const ChatsContext = createContext(initialValue);

export default ChatsContext;
