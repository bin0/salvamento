import React from 'react';
import PropTypes from 'prop-types';
import { CircularProgress } from '@material-ui/core';

const HelpView = ({ isLoading, i18n }) => {
  if (isLoading) {
    return <CircularProgress thickness={0.8} size={60} color="secondary" />;
  }
  return (
    <div>
      <p>{i18n.get('help.title')}</p>
    </div>
  );
};

HelpView.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  })
};

export default HelpView;
