import { connect } from 'react-redux';
import HelpView from '../components/HelpView';

const mapStateToProps = ({ loadingBar, i18n }) => ({
  isLoading: !!loadingBar.default,
  i18n
});

export default connect(mapStateToProps)(HelpView);
