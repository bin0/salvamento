import React, { useState, createRef } from 'react';
import {
  Card,
  CardHeader,
  CardContent,
  TextField,
  CardActions,
  Button,
  FormGroup,
  Avatar
} from '@material-ui/core';
import PropTypes from 'prop-types';
import defaultAvatarImg from '../../../assets/img/avatar.png';
import { useEffect } from 'react';

const OptionsView = ({
  avatar: userAvatar,
  displayName,
  i18n,
  isLoading,
  updateAvatar,
  updateOptions
}) => {
  const avatarRef = createRef();
  const [name, setName] = useState(displayName);
  const [avatar, setAvatar] = useState(defaultAvatarImg);

  useEffect(() => {
    if (userAvatar && userAvatar.data) {
      const data = Buffer.from(userAvatar.data).toString('base64');
      const contentType = userAvatar.data.contentType;
      setAvatar(`data:${contentType};base64,${data}`);
    }
  }, [userAvatar]);

  const handleChangeName = e => {
    setName(e.target.value);
  };

  const handleChangeAvatar = e => {
    const file = e.target.files[0];
    if (file.size > 100000) {
      return;
    }
    updateAvatar(e.target.files[0]);
  };

  const handleUpdateOptions = () => {
    if (!name) {
      return;
    }
    updateOptions({ displayName: name });
  };

  return (
    <Card>
      <CardHeader title="Opciones de usuario" subheader="Configura tus opciones" />
      <CardContent>
        <FormGroup style={{ marginBottom: '1rem' }}>
          <Avatar src={avatar} onClick={() => avatarRef.current.click()} alt="user-profile-img" />
          <input
            accept="image/png, image/jpeg"
            hidden
            id="avatarFile"
            onChange={handleChangeAvatar}
            ref={avatarRef}
            type="file"
          />
        </FormGroup>
        <FormGroup>
          <TextField label="Nombre de usuario" value={name} onChange={handleChangeName} />
        </FormGroup>
      </CardContent>
      <CardActions>
        <Button
          color="primary"
          disabled={isLoading}
          onClick={handleUpdateOptions}
          variant="contained"
        >
          {i18n.get('modify')}
        </Button>
      </CardActions>
    </Card>
  );
};

OptionsView.propTypes = {
  displayName: PropTypes.string.isRequired,
  updateOptions: PropTypes.func.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  })
};

export default OptionsView;
