import { connect } from 'react-redux';
import OptionsView from '../components/OptionsView';
import { actions as optionsActions } from '../modules/options';

const mapStateToProps = ({ user, loadingBar, i18n }) => ({
  displayName: user.local.displayName,
  avatar: user.local.avatar,
  isLoading: !!loadingBar.default,
  i18n
});

const mapDispatchToProps = { ...optionsActions };

export default connect(mapStateToProps, mapDispatchToProps)(OptionsView);
