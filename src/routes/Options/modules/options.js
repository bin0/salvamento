import createReducer from '../../../redux/create-reducer';
import axios from '../../../config/axios';
import { UPDATE_USER_OPTIONS, UPDATE_USER_AVATAR } from '../../../action-types';
import { getUser } from '../../../reducers/user/local';

const updateOptions = (options) => async (dispatch) => {
  dispatch({ type: UPDATE_USER_OPTIONS.REQUEST });

  try {
    const URL = 'user/options';
    await axios.patch(URL, options);

    dispatch({ type: UPDATE_USER_OPTIONS.SUCCESS });
    dispatch(getUser());
  } catch (err) {
    console.error(err);
    dispatch({ type: UPDATE_USER_OPTIONS.FAILURE });
  }
};

const updateAvatar = (file) => async (dispatch) => {
  dispatch({ type: UPDATE_USER_AVATAR.REQUEST });

  try {
    const formData = new FormData();
    formData.append('avatar', file);
    const URL = 'user/avatar';
    await axios.patch(URL, formData, { headers: { 'Content-Type': 'multipart/form-data' } });

    dispatch({ type: UPDATE_USER_AVATAR.SUCCESS });
    dispatch(getUser());
  } catch (err) {
    console.error(err);
    dispatch({ type: UPDATE_USER_AVATAR.FAILURE });
  }
};

export const actions = {
  updateOptions,
  updateAvatar,
};

export const INITIAL_STATE = {};

export const ACTION_HANDLERS = {};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
