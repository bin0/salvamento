import createReducer from '../../../redux/create-reducer';
import { SET_TEAMS } from '../../../action-types';
import { openSnack } from '../../../reducers/snack';
import axios from '../../../config/axios';
import history from '../../../redux/history';

const setTeams = (type, payload) => dispatch => dispatch({ type, payload });

const getUserTeams = () => async (dispatch, getState) => {
  const { _id } = getState().user.local;

  dispatch(setTeams(SET_TEAMS.REQUEST));

  try {
    const URL = `teams/user/${_id}`;
    const { data } = await axios(URL);

    if (data) {
      dispatch(openSnack(`Tienes ${data.length} equipos.`));
      dispatch(setTeams(SET_TEAMS.SET, data));
    }

    dispatch(setTeams(SET_TEAMS.SUCCESS));
  } catch (err) {
    dispatch(setTeams(SET_TEAMS.FAILURE));
  }
};

const getTeams = () => async dispatch => {
  dispatch(setTeams(SET_TEAMS.REQUEST));

  try {
    const URL = 'teams';
    const response = await axios(URL);

    dispatch(setTeams(SET_TEAMS.SUCCESS));
    dispatch(setTeams(SET_TEAMS.SET, response.data));
  } catch (err) {
    dispatch(setTeams(SET_TEAMS.FAILURE));
  }
};

const createTeam = team => async (dispatch, getState) => {
  const { _id } = getState().user.local;

  dispatch(setTeams(SET_TEAMS.REQUEST));

  try {
    const URL = `teams/${_id}`;
    const { data } = await axios.post(URL, { ...team, user: _id });

    history.push(`team/${data._id}`);
    dispatch(setTeams(SET_TEAMS.SUCCESS));
    dispatch(getTeams());
  } catch (err) {
    dispatch(setTeams(SET_TEAMS.FAILURE));
  }
};

export const deleteTeam = team => async dispatch => {
  dispatch(setTeams(SET_TEAMS.REQUEST));

  try {
    const URL = `teams/${team._id}`;
    await axios.delete(URL);

    dispatch(getTeams());
    dispatch(setTeams(SET_TEAMS.SUCCESS));
  } catch (err) {
    dispatch(setTeams(SET_TEAMS.FAILURE));
  }
};

export const actions = {
  getTeams,
  createTeam,
  getUserTeams,
  deleteTeam
};

export const INITIAL_STATE = {
  data: [],
  error: ''
};

export const ACTION_HANDLERS = {
  [SET_TEAMS.REQUEST]: () => INITIAL_STATE,
  [SET_TEAMS.SET]: (state, { payload }) => ({ ...state, data: payload }),
  [SET_TEAMS.FAILURE]: (state, { payload }) => ({ ...state, error: payload })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
