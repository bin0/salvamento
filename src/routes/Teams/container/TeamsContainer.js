import { connect } from 'react-redux';
import TeamsView from '../components/TeamsView';
import { actions as teamsActions } from '../modules/teams';

const mapStateToProps = ({ teams, loadingBar, i18n, user }) => ({
  teams: teams.data,
  isLoading: !!loadingBar.default,
  i18n,
  userId: user.local._id
});

const mapDispatchToProps = { ...teamsActions };

export default connect(mapStateToProps, mapDispatchToProps)(TeamsView);
