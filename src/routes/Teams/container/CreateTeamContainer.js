import { connect } from 'react-redux';
import CreateTeamView from '../components/CreateTeamView';
import { actions as teamsActions } from '../modules/teams';

const mapStateToProps = ({ loadingBar, i18n }) => ({
  isLoading: !!loadingBar.default,
  i18n
});

const mapDispatchToProps = { ...teamsActions };

export default connect(mapStateToProps, mapDispatchToProps)(CreateTeamView);
