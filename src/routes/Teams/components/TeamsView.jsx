import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import DeleteIcon from '@material-ui/icons/Delete';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import {
  CircularProgress,
  Button,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Card,
  CardActions,
  CardHeader,
  ListItemIcon
} from '@material-ui/core';

const Error = props => <div>{props.children}</div>;

const TeamsView = ({ deleteTeam, getTeams, history: { push }, i18n, isLoading, teams, userId }) => {
  const isOwner = useCallback(id => userId === id, [userId]);
  const handleClickTeam = useCallback(teamId => push(`teams/team/${teamId}`), [push]);
  const handleDeleteTeam = useCallback(team => deleteTeam(team), [deleteTeam]);

  useEffect(() => {
    getTeams();
  }, [getTeams]);

  if (isLoading || !teams) {
    return <CircularProgress thickness={0.8} size={60} color="secondary" />;
  }
  if (teams.error) {
    return <Error>{teams.error}</Error>;
  }

  return (
    <Card>
      <CardHeader title="Equipos" subheader="Selecciona un equipo para ver más opciones." />
      <List component="nav" aria-label="secondary mailbox folder">
        {teams.length > 0 ? (
          teams.map(team => (
            <ListItem key={team._id} button onClick={() => handleClickTeam(team._id)}>
              <ListItemIcon>
                <SupervisorAccountIcon />
              </ListItemIcon>
              <ListItemText primary={team.name} secondary={`Miembros: ${team.members.length}`} />
              <ListItemSecondaryAction>
                {isOwner(team.user) && (
                  <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteTeam(team)}>
                    <DeleteIcon />
                  </IconButton>
                )}
              </ListItemSecondaryAction>
            </ListItem>
          ))
        ) : (
          <p>{i18n.get('teams.noActiveTeams')}</p>
        )}
      </List>
      <CardActions>
        <Button variant="contained" color="secondary" onClick={() => push('/teams/new')}>
          {i18n.get('teams.addNewTeam')}
        </Button>
      </CardActions>
    </Card>
  );
};

TeamsView.propTypes = {
  teams: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  }),
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }),
  deleteTeam: PropTypes.func.isRequired,
  getTeams: PropTypes.func.isRequired
};

export default TeamsView;
