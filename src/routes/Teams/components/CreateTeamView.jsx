import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import { FormGroup, TextField, Card, CardHeader, CardActions, Button, CardContent } from '@material-ui/core';

const CreateTeamView = ({ i18n, createTeam, isLoading }) => {
  const [name, setName] = useState('');

  const handleCreateTeam = useCallback(() => {
    createTeam({ name });
  }, [createTeam, name]);

  const handleKeyDown = event => {
    if (event.key === 'Enter') {
      handleCreateTeam();
    }
  };

  return (
    <Card>
      <CardHeader title="¿Tienes un equipo?" subheader="Completa el formulario y da de alta tu propio equipo" />
      <CardContent>
        <FormGroup>
          <TextField
            autoFocus
            color="secondary"
            onKeyDown={handleKeyDown}
            variant="outlined"
            label="Nombre"
            value={name}
            onChange={e => setName(e.target.value)}
          />
        </FormGroup>
      </CardContent>
      <CardActions>
        <Button
          disabled={name.length < 3 || isLoading}
          variant="contained"
          color="secondary"
          onClick={handleCreateTeam}
        >
          {i18n.get('teams.addNewTeam')}
        </Button>
      </CardActions>
    </Card>
  );
};

CreateTeamView.propTypes = {
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  }),
  createTeam: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired
};

export default CreateTeamView;
