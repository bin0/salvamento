import createReducer from '../../../../../redux/create-reducer';
import { SET_TEAM } from '../../../../../action-types';
import axios from '../../../../../config/axios';
import { getUser } from '../../../../../reducers/user/local';

const setTeam = (type, payload) => dispatch => dispatch({ type, payload });

export const getTeam = teamId => async dispatch => {
  dispatch(setTeam(SET_TEAM.REQUEST));

  try {
    const URL = `teams/${teamId}`;
    const response = await axios(URL);

    dispatch(setTeam(SET_TEAM.SUCCESS, response.data));
  } catch (err) {
    dispatch(setTeam(SET_TEAM.FAILURE));
  }
};

export const requestTeamAccess = teamId => async (dispatch, getState) => {
  dispatch(setTeam(SET_TEAM.REQUEST));

  try {
    const userId = getState().user.local._id;
    const URL = `teams/join/${teamId}`;
    const { data: newTeam } = await axios.patch(URL, { userId });

    dispatch({ type: SET_TEAM.SUCCESS, payload: newTeam });
    dispatch(getUser());
  } catch (err) {
    dispatch(setTeam(SET_TEAM.FAILURE, err));
  }
};

export const leaveTeam = teamId => async (dispatch, getState) => {
  dispatch(setTeam(SET_TEAM.REQUEST));

  try {
    const userId = getState().user.local._id;
    const URL = `teams/leave/${teamId}`;
    const { data: newTeam } = await axios.patch(URL, { userId });

    dispatch({ type: SET_TEAM.SUCCESS, payload: newTeam });
    dispatch(getUser());
  } catch (err) {
    dispatch(setTeam(SET_TEAM.FAILURE, err));
  }
};

export const actions = {
  getTeam,
  requestTeamAccess
};

export const INITIAL_STATE = {
  data: {},
  error: '',
  isLoading: false
};

export const ACTION_HANDLERS = {
  [SET_TEAM.REQUEST]: () => INITIAL_STATE,
  [SET_TEAM.SUCCESS]: (state, { payload }) => ({ ...state, data: payload }),
  [SET_TEAM.FAILURE]: (state, { payload }) => ({ ...state, error: payload }),
  [SET_TEAM.UPDATE]: state => ({ ...state, isLoading: !state.isLoading })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
