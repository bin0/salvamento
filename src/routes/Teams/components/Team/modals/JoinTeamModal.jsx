import React, { createRef, useCallback, useMemo } from 'react';
import PropTypes from 'prop-types';
import CloseIcon from '@material-ui/icons/Close';
import { useDispatch, useSelector } from 'react-redux';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  TextField,
  MenuItem,
  Typography,
  IconButton,
  withStyles
} from '@material-ui/core';
import { requestTeamAccess } from '../modules/team';
import { capitalize } from '../../../../../utils/strings';

const formElements = ['disponibilidad', 'edad', 'fisico', 'ropa', 'experiencia', 'familia', 'equipo', 'cartografia'];
const formOptions = [
  { key: '', value: '' },
  { key: 'yes', value: 'Si' },
  { key: 'no', value: 'No' }
];
const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  textField: {
    marginBottom: theme.spacing(1),
    '&:last-child': {
      marginBottom: 0
    }
  }
});

const CustomDialogTitle = ({ classes, children, onClose }) => {
  return (
    <DialogTitle disableTypography>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

const JoinTeamModal = ({ close, classes, show, teamId }) => {
  const i18n = useSelector(({ i18n }) => i18n);
  const teamFormRef = createRef();
  const dispatch = useDispatch();
  const handleClose = useCallback(() => {
    close();
  }, [close]);

  const selectOptions = useMemo(
    () => ({ key, value }) =>
      key && (
        <MenuItem key={key} value={i18n.get(value)}>
          {value}
        </MenuItem>
      ),
    [i18n]
  );

  const handleSubmit = e => {
    //! FIXME ADOLFO Do something with form data
    // const { elements } = teamFormRef.current;
    // const data = formElements.reduce(
    //   (prev, next) => (elements[next].value ? { ...prev, [next]: elements[next].value } : prev),
    //   {}
    // );
    // console.log('data', data);

    dispatch(requestTeamAccess(teamId));
    close();
  };

  const renderTextField = classes => (name, index) =>
    name !== 'disponibilidad' && (
      <TextField
        autoFocus={index === 0}
        className={classes.textField}
        defaultValue=""
        fullWidth
        id={`${name}-select`}
        key={name}
        label={capitalize(name)}
        margin="dense"
        name={name}
        required
        variant="outlined"
      />
    );

  return (
    <Dialog open={show} aria-labelledby="join-team-dialog" onClose={handleClose}>
      <CustomDialogTitle classes={classes} onClose={handleClose}>
        Cuestionario beta para voluntarios
      </CustomDialogTitle>
      <DialogContent>
        <form onSubmit={handleSubmit} ref={teamFormRef} noValidate autoComplete="off">
          <TextField
            autoFocus
            className={classes.textField}
            defaultValue=""
            fullWidth
            id="disponibilidad-select"
            label="Disponibilidad"
            margin="dense"
            name="disponibilidad"
            select
            variant="outlined"
            required
          >
            {formOptions.map(selectOptions)}
          </TextField>
          {formElements.map(renderTextField(classes))}
        </form>
      </DialogContent>
      <DialogActions className={classes.root}>
        <Button type="submit" variant="contained" color="primary" onClick={handleClose}>
          cancelar
        </Button>
        <Button type="submit" variant="contained" color="secondary" onClick={handleSubmit}>
          enviar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

JoinTeamModal.propTypes = {
  close: PropTypes.func.isRequired,
  show: PropTypes.bool.isRequired,
  teamId: PropTypes.string.isRequired
};

export default withStyles(styles)(JoinTeamModal);
