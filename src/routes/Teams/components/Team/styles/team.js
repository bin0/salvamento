import styled from 'styled-components';

export const IconContainer = styled.div`
  margin-left: 1rem;
`;
