import { connect } from 'react-redux';
import TeamView from '../components/TeamView';
import { actions as teamActions } from '../modules/team';

const mapStateToProps = ({ team, loadingBar }) => ({
  name: team.data.name,
  description: team.data.description || '',
  isLoading: team.isLoading
});

const mapDispatchToProps = { ...teamActions };

export default connect(mapStateToProps, mapDispatchToProps)(TeamView);
