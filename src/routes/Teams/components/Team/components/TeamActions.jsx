import React, { useCallback } from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import { Button } from '@material-ui/core';
import { leaveTeam } from '../modules/team';
import { deleteTeam } from '../../../modules/teams';
import history from '../../../../../redux/history';
import { actions } from '../../../../../reducers/modal';

const TeamActions = ({ openModal }) => {
  const dispatch = useDispatch();
  const i18n = useSelector(({ i18n }) => i18n);
  const userId = useSelector(({ user }) => user.local._id);
  const { members, status, user, _id: teamId } = useSelector(({ team }) => team.data);
  const isTeamMember = members.some(m => m._id === userId);
  const isTeamOwner = user === userId;

  const handleRequestTeamAccess = useCallback(() => {
    openModal({ modalType: 'JOIN_TEAM_MODAL', modalProps: { teamId } });
  }, [openModal, teamId]);

  const handleLeaveTeam = useCallback(() => {
    dispatch(leaveTeam(teamId));
  }, [teamId, dispatch]);

  const handleRemoveTeam = useCallback(() => {
    dispatch(deleteTeam({ _id: teamId }));
    history.push('/teams');
  }, [teamId, dispatch]);

  return (
    <>
      {!isTeamMember && (
        <Button
          disabled={status !== 'open' || isTeamMember}
          variant="contained"
          color="secondary"
          onClick={handleRequestTeamAccess}
        >
          {i18n.get('team.requestAccess')}
        </Button>
      )}
      {isTeamMember && !isTeamOwner && (
        <Button variant="contained" color="secondary" onClick={handleLeaveTeam}>
          {i18n.get('exit')}
        </Button>
      )}
      {isTeamOwner && (
        <Button variant="contained" color="secondary" onClick={handleRemoveTeam}>
          {i18n.get('team.remove')}
        </Button>
      )}
    </>
  );
};

export default connect(null, { ...actions })(TeamActions);
