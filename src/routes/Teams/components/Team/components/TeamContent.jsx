import React, { memo } from 'react';
import { useSelector } from 'react-redux';
import { List, ListItemText } from '@material-ui/core';
import TeamMembersList from './TeamMembersList';

const TeamContent = () => {
  const i18n = useSelector(({ i18n }) => i18n);
  const team = useSelector(({ team }) => team.data);
  const userId = useSelector(({ user }) => user.local._id);
  const { locations, members, status } = team;
  const isUserIntoTeam = members.some(m => m._id === userId);
  const isTeamOwner = members.some(m => m.user === userId);

  return (
    <List>
      <ListItemText>{isTeamOwner && i18n.get('team.owner')}</ListItemText>
      <ListItemText>
        {i18n.get('team.status')}: <b>{i18n.get(status)}</b>
      </ListItemText>
      {locations.length > 0 && (
        <ListItemText>
          {i18n.get('team.location')}: {locations.length}{' '}
        </ListItemText>
      )}
      <ListItemText>
        {i18n.get('team.members')}: <b>{members.length} </b>
        {isUserIntoTeam && `(${i18n.get('team.userin')})`}
      </ListItemText>
      <TeamMembersList />
    </List>
  );
};

export default memo(TeamContent);
