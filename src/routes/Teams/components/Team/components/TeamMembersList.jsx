import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import { ListItem, ListItemText } from '@material-ui/core';
import { FixedSizeList as List } from 'react-window';
import history from '../../../../../redux/history';

const itemSize = 55;

const TeamMembersList = () => {
  const membersList = useSelector(({ team }) => team.data.members);
  const userId = useSelector(({ user }) => user.local._id);
  const removeSelf = useCallback(({ _id }) => _id !== userId, [userId]);
  const handleUserClick = useCallback(id => history.push(`/user/${id}`), []);
  const listSize = membersList.filter(removeSelf).length;
  const listHeight = listSize * itemSize > 300 ? 300 : listSize * itemSize;

  const Row = ({ index }) => {
    const item = membersList[index];
    return (
      <ListItem button key={item._id} onClick={() => handleUserClick(item._id)}>
        <ListItemText>{item.displayName}</ListItemText>
      </ListItem>
    );
  };

  return (
    <List height={listHeight} itemCount={listSize} itemSize={35} width={300}>
      {Row}
    </List>
  );
};

export default TeamMembersList;
