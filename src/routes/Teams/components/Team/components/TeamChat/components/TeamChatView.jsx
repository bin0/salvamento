import React, { useEffect, useCallback, createRef } from 'react';
import { useState } from 'react';
import {
  TextField,
  InputAdornment,
  IconButton,
  List,
  ListItemText,
  ListItemSecondaryAction,
  ListItem,
  withStyles
} from '@material-ui/core';
import Send from '@material-ui/icons/Send';
import DeleteIcon from '@material-ui/icons/Delete';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import styles from '../styles/teamChat';

function isValid(str) {
  return str && str.length > 3;
}

const TeamChat = ({
  initTeamChat,
  classes,
  deleteMessage,
  sendChatMessage,
  team,
  user,
  teamChat,
  isUserIntoTeam,
  isTeamOwner
}) => {
  const itemRef = createRef();
  const [message, setMessage] = useState('');
  const [showMessages, setShowMessages] = useState(false);
  const isMessageOwner = useCallback(id => id === user._id, [user._id]);
  const getMessageOwnerName = useCallback(
    messageOwnerId => {
      return team.members.find(m => m._id === messageOwnerId).displayName;
    },
    [team.members]
  );

  useEffect(() => {
    initTeamChat(team.chat);
  }, [initTeamChat, team.chat]);

  useEffect(() => {
    if (itemRef.current) {
      itemRef.current.scrollTop = itemRef.current.scrollHeight;
    }
  }, [itemRef]);

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };

  const handleClick = () => {
    if (isValid(message)) {
      sendChatMessage(message, user._id, team.chat);
      setMessage('');
    }
  };
  const handleChange = e => {
    const { value } = e.target;
    setMessage(value);
  };

  const handleDeleteMessage = useCallback(
    id => () => {
      const newMessages = teamChat.messages.filter(m => m._id !== id);
      console.log('newMessages', newMessages);
      deleteMessage(id, team.chat);
    },
    [deleteMessage, team.chat, teamChat.messages]
  );

  const handleShowMessages = useCallback(() => {
    setShowMessages(prev => !prev);
  }, []);

  const renderMessageList = useCallback(
    () =>
      teamChat.messages &&
      teamChat.messages.map(m => (
        <ListItem key={m._id}>
          <ListItemText>
            <b>{getMessageOwnerName(m.user)}</b>: {m.text}
          </ListItemText>
          <ListItemSecondaryAction>
            {(isMessageOwner(m.user) || isTeamOwner) && (
              <IconButton color="primary" edge="end" aria-label="delete" onClick={handleDeleteMessage(m._id)}>
                <DeleteIcon fontSize="small" />
              </IconButton>
            )}
          </ListItemSecondaryAction>
        </ListItem>
      )),
    [getMessageOwnerName, handleDeleteMessage, isMessageOwner, isTeamOwner, teamChat.messages]
  );

  return (
    isUserIntoTeam && (
      <>
        <List>
          <ListItemText>
            Chat de equipo
            <ListItemSecondaryAction>
              <IconButton edge="end" aria-label="delete" onClick={handleShowMessages}>
                {showMessages ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </ListItemSecondaryAction>
          </ListItemText>
        </List>
        {showMessages && (
          <>
            <List className={classes.chatList} ref={itemRef}>
              {renderMessageList()}
            </List>
            <TextField
              autoFocus
              color="secondary"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      color="secondary"
                      aria-label="send chat message"
                      onClick={handleClick}
                      onMouseDown={handleMouseDownPassword}
                    >
                      <Send />
                    </IconButton>
                  </InputAdornment>
                )
              }}
              fullWidth
              onChange={handleChange}
              placeholder="Escribe tu mensaje..."
              variant="standard"
              value={message}
            />
          </>
        )}
      </>
    )
  );
};

export default withStyles(styles)(TeamChat);
