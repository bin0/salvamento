export default theme => ({
  chatList: {
    background: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    maxHeight: 200,
    overflowY: 'auto',
    marginBottom: theme.spacing(2),
    borderRadius: theme.spacing(1)
  }
});
