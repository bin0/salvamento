import createReducer from '../../../../../../../redux/create-reducer';
import socketIo from '../../../../../../../modules/socket';
import { SET_TEAM_CHAT_MESSAGES } from '../../../../../../../action-types';
import { setConnectedUsersCount } from '../../../../../../../reducers/users';

const socket = socketIo.teams;

const getMessages = chatId => dispatch => {
  socket.emit('get-messages', chatId);
};

const sendChatMessage = (message, userId, chatId) => dispatch => {
  socket.emit('new-message', chatId, message, userId);
};

const initTeamChat = chatId => (dispatch, getState) => {
  socket.removeAllListeners();
  const { email } = getState().user.local;

  socket.emit('user-connected', email);
  socket.emit('join-chat-room', chatId);

  socket.on(`set-messages-${chatId}`, msgs => {
    dispatch({ type: SET_TEAM_CHAT_MESSAGES.SET, payload: msgs });
  });

  socket.on('connected-users-count', count => {
    dispatch(setConnectedUsersCount(count));
  });

  dispatch(getMessages(chatId));
};

const deleteMessage = (messageId, chatId) => dispatch => {
  socket.emit('delete-message', messageId, chatId);
};

export const actions = {
  getMessages,
  initTeamChat,
  sendChatMessage,
  deleteMessage
};

export const INITIAL_STATE = {
  isLoading: false,
  messages: []
};

export const ACTION_HANDLERS = {
  [SET_TEAM_CHAT_MESSAGES.SET]: (state, { payload }) => ({ ...state, messages: payload })
};

export default createReducer(INITIAL_STATE, ACTION_HANDLERS);
