import { connect } from 'react-redux';
import TeamChatView from '../components/TeamChatView';
import { actions } from '../modules/team-chat';

const mapStateToProps = ({ team, user, teamChat }) => ({
  team: team.data,
  user: user.local,
  teamChat,
  isTeamOwner: team.data.user === user.local._id,
  isUserIntoTeam: team.data.members.some(m => m._id === user.local._id)
});

const mapDispatchToProps = { ...actions };

export default connect(mapStateToProps, mapDispatchToProps)(TeamChatView);
