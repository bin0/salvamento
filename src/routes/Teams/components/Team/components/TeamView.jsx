import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Card, CardHeader, CardContent, CardActions, CircularProgress } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import TeamContent from './TeamContent';
import TeamActions from './TeamActions';
import TeamChat from './TeamChat';
import { IconContainer } from '../styles/team';

const TeamView = ({
  getTeam,
  match: {
    params: { id: teamId }
  },
  name,
  description,
  isLoading
}) => {
  useEffect(() => {
    getTeam(teamId);
  }, [getTeam, teamId]);

  if (isLoading || !name) {
    return <CircularProgress thickness={0.8} size={60} color="secondary" />;
  }

  return (
    <Card>
      <CardHeader
        title={name}
        subheader={description || 'Descripción o briefing del equipo'}
        avatar={<SupervisorAccountIcon />}
        action={
          <IconContainer>
            <MenuIcon />
          </IconContainer>
        }
      />
      <CardContent>
        <TeamContent />
        <TeamChat />
      </CardContent>
      <CardActions>
        <TeamActions />
      </CardActions>
    </Card>
  );
};

TeamView.propTypes = {
  getTeam: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  }),
  name: PropTypes.string,
  description: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
  i18n: PropTypes.shape({
    get: PropTypes.func.isRequired
  })
};

export default TeamView;
