import React from 'react';
import { Route } from 'react-router-dom';
import TeamsContainer from './container/TeamsContainer';
import CreateTeamContainer from './container/CreateTeamContainer';
import TeamContainer from './components/Team/container/TeamContainer';

const ROUTE_PREFIX = '/teams';

export default () => (
  <div>
    <Route path={ROUTE_PREFIX} exact component={TeamsContainer} />
    <Route path={`${ROUTE_PREFIX}/new`} exact component={CreateTeamContainer} />
    <Route path={`${ROUTE_PREFIX}/team/:id`} exact component={TeamContainer} />
  </div>
);
