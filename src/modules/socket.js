import io from 'socket.io-client';

const ca = Buffer.from('certs/gbp.pem');
const token = localStorage.getItem('wc-token');

const socketOptions = {
  secure: true,
  path: '/gbpd-sk',
  ca,
  // passphrase: "mypassphrase",
  query: {
    token
  },
  // // option 2. WARNING: it leaves you vulnerable to MITM attacks!
  rejectUnauthorized: true
};

function initListeners(socket) {
  socket.on('connection', () => {});
  socket.on('disconnected', () => {});
  socket.on('connect', () => {});
  socket.on('connect_error', () => {});
  socket.on('connect_timeout', () => {});
  socket.on('error', () => {});
  socket.on('reconnect', () => {});
  socket.on('reconnect_attempt', () => {});
  socket.on('reconnecting', () => {});
}

const main = io.connect(process.env.REACT_APP_API_URL, socketOptions);
initListeners(main);

const teams = io(`${process.env.REACT_APP_API_URL}/teams`);
initListeners(teams);

export default {
  main,
  teams
};
