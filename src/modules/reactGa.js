import ReactGA from 'react-ga';

function handleReactGa(type, ...props) {
  if (process.env.NODE_ENV !== 'test') {
    // ReactGA[type](...props);alize must be called first
  } else {
    if (process.env.NODE_ENV === 'production') {
      ReactGA[type](...props);
    }
  }
}

export function sendPage(pathname) {
  handleReactGa('pageview', pathname, []);
  handleReactGa('event', {
    category: 'User',
    action: `Navigates to ${pathname}`,
    value: 1,
    label: 'Page Visited',
    nonInteraction: true
  });
}

export default handleReactGa;
