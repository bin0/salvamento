export const campaigns = () => {
  return [
    {
      id: 1,
      name: 'Nani Sagunto',
      openDate: new Date('2019-08-04'),
      buscadores: [],

      dissapeared: {
        name: 'Guest',
        lastSeen: new Date(),
        lastLocation: {
          lat: 39.6806473,
          lng: -0.285634
        }
      },
      startPoints: [
        {
          startDate: new Date(),
          endDate: new Date(),
          location: {
            lat: 39.6806473,
            lng: -0.285634
          },
          isMain: true
        }
      ]
    }
  ];
};
