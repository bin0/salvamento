import React, { useEffect } from 'react';
import { campaigns as mocks } from '../mocks/campaigns';

function useCampaign() {
  const [campaigns, setCampaigns] = React.useState([]);

  useEffect(() => {
    setCampaigns(mocks());
  }, []);

  return {
    campaigns
  };
}

export default useCampaign;
