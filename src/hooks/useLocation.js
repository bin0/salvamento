import { useState, useReducer, useCallback } from 'react';

const options = {
  enableHighAccuracy: false,
  timeout: 10000,
  maximumAge: 0
};

function reducer(prevState, nextState) {
  return { ...prevState, ...nextState };
}

function useLocation() {
  const [location, setLocation] = useReducer(reducer, {});
  const [watching, setWatching] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  function handleNewLocation(loc) {
    const coords = {
      lat: loc.coords.latitude,
      lng: loc.coords.longitude
    };
    setLocation(coords);
    localStorage.setItem('lastLocation', JSON.stringify(coords));
    setLoading(false);
  }

  function handleErrorLocation(err) {
    setError(err);
  }

  const watchPosition = useCallback(() => {
    setLoading(true);
    if (navigator.geolocation && navigator.permissions) {
      navigator.permissions.query({ name: 'geolocation' }).then(({ state }) => {
        if (state === 'granted' || state === 'prompt') {
          setWatching(navigator.geolocation.watchPosition(handleNewLocation, handleErrorLocation, options));
        }
      });
    }
  }, []);

  const clearWatching = useCallback(() => {
    navigator.geolocation.clearWatch(watching);
    setWatching(null);
  }, [watching]);

  return {
    loading,
    location,
    watching,
    watchPosition,
    clearWatching,
    error
  };
}

export default useLocation;
