import React, { useEffect } from 'react';
import { users as mocks } from '../mocks/users';

function useCampaign() {
  const [user, setuser] = React.useState([]);

  useEffect(() => {
    const number = Math.floor(Math.random);
    console.log('number', number);
    setuser(mocks()[number]);
  }, []);

  return {
    user
  };
}

export default useCampaign;
