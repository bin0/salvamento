This project was bootstrapped with [Create React App](/CRA.md).

# Where to start

### [Milestones](https://gitlab.com/onrubia78/salvamento/-/milestones)

### [Issues](https://gitlab.com/onrubia78/salvamento/issues)

Select an issue to start from, then create a branch from it, or even an MR with a branch from `dev`.

This way now.sh will serve an autodeploy for this branch after each push to our issue branch.

# Branches

- ### `dev`

  - **Branch to start from**

- `release/vx.x.x`
- `release/vx.x.x-stable`
- `release/vx.x.x-production`

- ### `master`
  - Production branch (Protected)

## GitFlow

feature \
bugfix \
hotfix \
...

VSCODE
prettier rules
eslint

# Deploys

It runs in now.sh

### Autodeploys

Will run in every branch pushed within a MR to dev.

Create a branch from an issue

# i18n

i18n are server from redux as `i18n` reducer (state.i18n)

It has a `get` method and receives one string parameter with the key to translate

### Example

container:

```js
const mapStateToProps = ({ i18n }) => ({ i18n });
```

view:

```js
<p>{this.props.i18n.get('my.id')}</p>
```

# Modals

Are setup in redux
Will expose 2 methos `openModal` y `closeModal`.

### Example

use:

```jsx
import { openModal } from '../reducers/modal';

class MyReactClass extends React.Component {
  state = {
    foo: 'bar'
  };

  handleShowModal = () => {
    this.props.openModal({
      modalType: 'MY_MODAL_KEY',
      modalProps: { ...this.state }
    });
  };
  render() {
    return !this.props.isModalOpen && <button onClick={handleShowModal}>ShowModal</button>;
  }
}
```

container:

```js
const mapStateToProps = ({ modal }) => ({ isModalOpen: modal.show });

const mapDispatchToProps = { openModal, closeModal };

export default connect(mapStateToProps, mapDispatchToProps)(MyReactClass);
```

# Routes

Handler `src/routes/routes.js`

## Without auth

```jsx
<RouteWithLayout
    key="/"
    path="/"
    exact
    component={Home}
    layout={MainLayout}
  />,
```

## With auth

```jsx
  <RouteWithLayout
    private={authenticated}
    key="options"
    path="/options"
    exact
    component={AuthWrapper(Options)}
    layout={MainLayout}
  />,
```

# CI

now deploy .b command

```-yml
--build-env REACT_APP_API_VERSION=$REACT_APP_API_VERSION --build-env REACT_APP_VERSION=$REACT_APP_VERSION --build-env REACT_APP_GA_ID=$REACT_APP_GA_ID --build-env REACT_APP_GOOGLE_MAPS_API_KEY=$REACT_APP_GOOGLE_MAPS_API_KEY --build-env REACT_APP_SOCKET_URL=$REACT_APP_SOCKET_URL --build-env REACT_APP_API_URL=$REACT_APP_API_URL --build-env REACT_APP_ENV_NAME=$REACT_APP_ENV_NAME
```

si eres medico o conocimientos sanitarios
piloto dron (licencia) (profesional)
miembro fuerza de seguroidad del estado
tienes perro y perteneces a grupo
